<script type="text/javascript">
		$(document).ready(function() {
				setInterval(function() {
						location.reload();
                }, 30000);
        });

</script>
<?php $order_detail = $this->session->userdata('order_detail'); ?>
<div class="page-content">
      <!-- end .map-section -->
      <div class="contact-us">
        <div class="container">
          <div class="row">
			 <div class="col-md-6">
              <div class="send-message">
			  
			  <?php 
			  
			  
	
			  if($order_status['status'] == 2) {
					echo "<p style='color:red'> Order Accepted. Thank you</p> "; 
					$this->session->unset_userdata('order_id');
			  } else  if($order_status['status'] == 1)  { 
					$this->session->unset_userdata('order_id');
					echo "<p style='color:red'> Order Rejected! Please try latter.</p>";
			  }  else { ?>
				<p style="color:red" id="orders_status">  <?php echo $this->session->flashdata('msg'); ?>please wait .... Order ID : <?php echo $this->session->userdata('current_order'); ?> </p>
			 <?php  } ?>
			 
			 <?php if($this->session->userdata('order_id')) {  ?>
  <h4 align="center">Your Order Number</h4>
				<table width="100%"> 
				<?php if ($cart_detail) { $total_price = 0; ?>
					<?php foreach ($cart_detail as $key => $item) { ?> 
					<tr> 
						<td width="70%"> <?php echo $item->name; ?> </td>
						<td>   <?php echo $item->qty . " * " . $item->price; ?> </td>
					</tr>
					
					<?php $total_price = $total_price + ($item->price * $item->qty); } ?>
				<?php } ?>
				<tr> 
					
				</tr>
				<tr style="border-top:1px solid"> 
					<th> TOTAL </th>
					<td><?php echo number_format($total_price , 2); ?>&pound;</td>
				</tr> 
				
				<tr style="border-top:1px solid"> 
					<td colspan="2"> <?php  
						echo $order_detail['name'] . "<br />";
						echo $address_detail['addressLine1'] . "<br />";
						echo $order_detail['mobile_number'] . "<br />";
					?> </td>
				</tr>
				
				<tr style="border-top:1px solid"> 
					<td colspan="2"> Delivery Request for <u><?php echo  $order_detail['d_time'] ?> </u></td>
				</tr>
				
				
				</table>
				
				<?php } ?>
				
              </div>
              <!-- end .send-message -->
            </div>
            <!-- end .main-grid-layout -->
			
			
			
			<div class="col-md-6">
              <div class="send-message">
                <h4 align="center">Your Previous Orders</h4>
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
				<?php if ($previous_orders) {  ?>
					<?php foreach ($previous_orders as $key => $item) {
							if($this->session->userdata('current_order') != $item['order_id']) { 
					?> 
					<div class="panel panel-default">
					<div class="panel-heading" role="tab" id="headingOne">
					  <h6 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $key; ?>" aria-expanded="true" aria-controls="collapse<?php echo $key; ?>">
						<?php echo $item['order_id']; ?> <small style="color:blue"> ( <?php echo date('d M Y' , strtotime($item['created_at'])); ?> ) </small>
						<small style="float:right"> <?php if($item['status'] == 0 ) { echo "Open"; } else if($item['status'] == 1) { echo "Recieved"; } else { echo "Completed"; }  ?> </small>
						</a>
					</h6>
					 </div>
					  <div id="collapse<?php echo $key; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
					  <div class="panel-body">
							<?php foreach($item['items'] as $i) {  ?>
								<div> <?php echo $i['menu_item_name'] . " -----  <strong>" . $i['price'] . " * " . $i['quantity'] . "</strong>"; ?> </div>
							<?php } ?>
					  </div>
					</div>
					</div>
				<?php } } } else {  ?>
				 <p> No Record Found  </p>
				<?php }?>
			
				</div>
              <!-- end .send-message -->
            </div>
            </div>
            <!-- end .main-grid-layout -->
			
		
          </div>
          <!-- end .row -->
        </div>
        <!-- end .container -->
      </div>
      <!-- end .contact-us -->
    </div>
    <!-- end page-content -->