
<div class="side-panel">
    <h6 class="toggle-main-title">Category</h6>
    <!-- end .sd-side-panel class -->
    <div class="category">
        <ul class="list-unstyled">
            <li <?php if($this->uri->segment(3)=='' || $this->uri->segment(3)=='featured'){echo 'class="active"';}?>> 
                <i class="fa fa-angle-double-right"></i><?php echo anchor('takeaway/menu/featured', 'Featured Items'); ?>
            </li>
            <?php foreach ($categories as $category) { ?>
                <li <?php if($this->uri->segment(3)==$category['cat_slug'] ){echo 'class="active"';}?>>
                    <i class="fa fa-angle-double-right"></i> <?php echo anchor('takeaway/menu/' . $category['cat_slug'], $category['category_name']); ?> 
                </li>
            <?php } ?>
        </ul>
    </div>
    <!--end .category-->
</div>