<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo $title . ' | ' . $this->config->item('site_title'); ?></title>
        <!-- Stylesheets -->
        <link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>

        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/bootstrap.css">
        <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>js/masterslider/style/masterslider.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>js/masterslider/skins/black-2/style.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/style.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/owl.theme.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url() . 'assets/'; ?>css/responsive.css">






        <!--[if lt IE 9]>
                <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
                <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <script type="text/javascript">
            var add_markar_object = {'position': '<?php echo $venues['latitude']; ?> , <?php echo $venues['longitude']; ?> ', 'bounds': true};
            var content_object = {'content': '<?php echo $venues['name']; ?>'};
        </script>


    </head>
    <body>
        <div id="main-wrapper">
            <header id="header">
                <div class="header-top-bar">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4 col-sm-5 col-xs-12 top-padding">
                                <div class="header-login">
                                    <?php
                                    $logged_user = $this->session->userdata('logged_user');
                                    //print_r($logged_user); exit;
                                    if ($logged_user['is_logedin'] == true) {
                                        ?>
                                        <a   href="#modal-profile" data-toggle="modal" > <?php echo "Welcome " . $logged_user['name']; ?> </a>
                                        <?php
                                    } else {
                                        echo anchor('user/register', 'Register');
                                    }
                                    ?>

                                    <?php
                                    if ($logged_user['is_logedin'] == true) {
                                        echo anchor('user/logout', 'Logout');
                                    } else {
                                        ?> 
                                        <a href="#modal-login" data-toggle="modal"> Login </a>
                                    <?php } ?>
                                </div>
                                <!-- end .header-login -->
                                <!-- Header Social -->
                                <ul class="header-social">
                                    <li><a href="<?php //echo $venues['facebook'];              ?>" target="_blank"><i class="fa fa-facebook-square"></i></a> </li>
                                    <li><a href="<?php //echo $venues['twitter'];              ?>" target="_blank"><i class="fa fa-twitter-square"></i></a> </li>                                  
                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-2 col-xs-12">
                                <a class="navbar-brand" href="<?php echo base_url() ?>">
                                    <img src="<?php echo base_url() ?>assets/img/header-logo.png" alt="<?php echo $venues['name']; ?>">
                                </a>
                            </div>
                            <div class="col-md-4 col-sm-5 col-xs-12 hidden-xs top-padding">
                                <p class="call-us">
                                    <span class="open-now"><i class="fa fa-check-square"></i>We are open now(<?php echo $venues['opening_time'] . ' - ' . $venues['closing_time']; ?>)</span>
                                    <span class="close-now"><i class="fa fa-square"></i>We are close now(<?php echo $venues['closing_time'] . ' - ' . $venues['opening_time']; ?>)</span>
                                </p>
                            </div>
                        </div> <!-- end .row --> 
                    </div> <!-- end .container -->
                </div>
                <!-- end .header-top-bar -->

                <div class="header-nav-bar">
                    <nav class="navbar navbar-default" role="navigation">
                        <div class="container">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>

                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav navbar-left">
                                    <li class="dropdown active1">
                                        <a href="<?php echo base_url() ?>">Home</a>
                                    </li>

                                    <li><a href="<?php echo site_url('takeaway/menu') ?>">Menu</a>
                                    </li>
                                   <!-- <li><a href="<?php // echo site_url('takeaway/checkout')              ?>">Checkout</a> -->
                                    <li><a  href="<?php echo site_url('takeaway/checkout')              ?>">Checkout</a>
                                    </li>
                                    <li><a href="<?php echo site_url('takeaway/opening_times') ?>">Opening Times</a>
                                    <li><a href="<?php echo site_url('takeaway/book_a_table') ?>">Book Table</a>
                                    <li><a href="<?php echo site_url('takeaway/contact') ?>">Contact us</a>
                                    </li>
                                </ul>
                            </div>
                            <!-- /.navbar-collapse -->
                        </div>
                        <!-- /.container-fluid -->
                    </nav>
                </div>
                <!-- end .header-nav-bar -->

            </header>
            <!-- end #header -->

            <!-- all page-content star -->
            <div id="page-content">
                <!-- masterslider -->
                <div class="master-slider ms-skin-black-2 round-skin" id="masterslider">
                    <?php foreach ($slides as $slide) { ?> 
                        <!-- new slide -->
                        <div class="ms-slide">
                            <!-- slide background -->
                            <img src="<?php echo base_url() ?>assets/js/masterslider/blank.gif" data-src="<?php echo $slide['image_o'] ?>" alt="">
                            <!-- slide text layer -->
                            <div class="ms-layer ms-caption" style="">
                                <h1 class="text-right">
                                    <span><?php echo $slide['title']; ?></span><br>
                                    <?php echo $slide['desc']; ?>
                                </h1>
                            </div>
                        </div>
                        <!-- end of slide -->
                    <?php } ?>
                </div>
                <!-- end of masterslider -->

                <!-- purchase TakeAway section start -->
                <div class="container-fluid order-bg">
                    <div class="container">
                        <div class="call-to-action-section">
                            <div class="css-table-cell">
                                <div class="icon">
                                    <img src="<?php echo base_url() ?>assets/img/content/call-to-action-icon1.png" alt="">
                                </div>
                            </div>
                            <div class="text css-table">
                                <div class="css-table-cell">
                                    <h4><?php echo $homepage['welcome_title'] ?></h4>
                                    <p><?php echo $homepage['welcome_note'] ?></p>
                                </div>                         
                                <div class="css-table-cell">
                                    <a href="<?php echo site_url('takeaway/menu'); ?>" class="btn btn-default-red-inverse pad-top"><i class="fa fa-shopping-cart"></i> Order Now!</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- end .call-to-action-section -->
                </div>
                <!-- end .container -->
                <!-- end purchase TakeAway section  -->

                <!-- start .category box section -->
                <div class="category-boxes-icons">
                    <div class=" container">
                        <div class="row">

                            <?php foreach ($home_categories as $key => $list) { ?>
                                <div class="col-md-3 col-sm-6 col-xs-10 col-xs-offset-1 col-sm-offset-0 col-md-offset-0 text-center">
                                    <div class="category-boxes-item">
                                        <figure>
                                            <div class="frame">
                                                <img src="<?php echo base_url() ?>assets/img/content/frame.png" alt="">
                                            </div>
                                            <img src="<?php echo $list['image_s']; ?>">
                                            <h4><?php echo $list['category_name']; ?></h4>
                                            <figcaption> <a href="<?php echo site_url('takeaway/menu/' . $list['cat_slug']); ?>" class="btn btn-default-white">View Menu</a> 
                                            </figcaption>
                                        </figure>

                                    </div>
                                </div>
                                <?php
                                if ($key == 3) {
                                    break;
                                }
                                ?>
                            <?php } ?>

                        </div>
                        <!-- end .row -->
                    </div>
                    <!-- end .category-boxes-icons -->
                </div>
                <!-- container -->

                <!-- star.chef-welcome -->
                <div class="chef-welcome">
                    <div class="container">
                        <div class="col-md-7 col-sm-12">
                            <h1><?php echo $homepage['intro_title'] ?></h1>
                            <p><?php echo $homepage['intro_desc'] ?></p>
                        </div>
                        <div class="col-md-5 col-sm-12">
                            <img src="<?php echo base_url() ?>assets/img/content/thali.png" class="img-responsive"/>
                        </div>
                    </div>
                    <!-- end .container -->
                </div>

                <!--end .chef-welcome-->
                <?php if (count($testimonials) > 0) { ?>
                    <!--start small-slide section -->
                    <div id="sm-slide-section">
                        <div class="container">
                            <div class="slide-heading text-center">
                                <h4>Our Clients Say</h4>
                            </div>
                            <!--end .clients heading-->
                            <div id="slide-content" class="owl-carousel">
                                <?php foreach ($testimonials as $item) { ?>
                                    <div class="item">
                                        <img src="<?php echo $item['image_s'] ?>" alt="<?php echo $item['name'] ?>">
                                        <div class="details">
                                            <h5><a href="#"><?php echo $item['name'] ?></a> </h5>
                                            <p><?php echo $item['text'] ?></p>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                            <!-- end i.slide-content -->
                        </div>
                        <!-- end .container-->
                    </div>
                    <!-- end .sm-slide-section-->
                <?php } ?>
                <?php if (count($events) > 0) { ?>    
                    <!--Start events feed section-->
                    <div class="latest-from-blog text-center">
                        <div class="container">
                            <h4>Our Specials</h4>
                            <div class="row">
                                <?php foreach ($events as $key => $event) { ?>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                        <div class="blog-latest">
                                            <div class="row">
                                                <div class="col-md-6 col-sm-12">
                                                    <img class="" src="<?php echo $event['image_s'] ?>" alt="<?php echo $event['title'] ?>">
                                                </div>
                                                <div class="col-md-6 col-sm-12">
                                                    <h5><a href="#"><?php echo $event['title'] ?></a> </h5>

                                                    <p class="bl-sort"><?php echo substr($event['description'], 0, 70); ?></p>
                                                    <a href="<?php echo base_url(); ?>takeaway/book_a_table" class="btn btn-default-red-inverse pad-top">Book Table</a>
                                                    <!--<a href="#" class="btn btn-default-red"><i class="fa fa-file-text-o"></i> Read  More</a>-->
                                                </div>
                                                <!--end .events-details-->
                                            </div>
                                            <!--end .row-->
                                        </div>
                                        <!--end .events-latest -->
                                    </div>
                                    <!--end grid layout-->
                                    <?php
                                    if ($key == 3) {
                                        break;
                                    }
                                    ?>
                                <?php } ?>
                            </div>
                            <!--end .row main-->
                            <br />
                        </div>
                        <!--end container-->
                    </div>
                    <!--end .latest-from-events-->
                <?php } ?>
            </div>
            <!-- end #page-content -->
            <!--footer start-->
            <?php echo $this->load->view('theme/footer'); ?>    
            <!-- end #footer -->

        </div>
        <!-- end #main-wrapper -->
        <!-- Scripts -->
        <!-- CDN jQuery -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
        <!-- Local jQuery -->

        <script>
            window.jQuery || document.write('<script src="<?php echo base_url() . 'assets/' ?>js/jquery-1.11.0.min.js"><\/script>')
        </script>
        <script src="<?php echo base_url() . 'assets/'; ?>js/masterslider/masterslider.min.js"></script>
        <script src="<?php echo base_url() . 'assets/'; ?>js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
        <script src="<?php echo base_url() . 'assets/'; ?>js/jquery.magnific-popup.min.js"></script>
        <script src="<?php echo base_url() . 'assets/'; ?>js/owl.carousel.js"></script>
        <script src="<?php echo base_url() . 'assets/'; ?>js/bootstrap.js"></script>
        <script type="text/javascript" src="<?php echo base_url() . 'assets/' ?>js/jquery.ui.map.js"></script>
        <script src="<?php echo base_url() . 'assets/' ?>js/scripts.js"></script>
        <script>

            var slider = new MasterSlider();
            slider.setup('masterslider', {
                width: 1140,
                height: 500,
                space: 5,
                fullwidth: true,
                speed: 25,
                view: 'flow',
                centerControls: false
            });
            slider.control('bullets', {
                autohide: false
            });


        </script>
        <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"></script>

        <script>
            var myCenter = new google.maps.LatLng(<?php echo $venues['latitude']; ?>,<?php echo $venues['longitude']; ?>);

            function initialize()
            {
                var mapProp = {
                    center: myCenter,
                    zoom: 17,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

                var marker = new google.maps.Marker({
                    position: myCenter,
                    title: "<?php echo $venues['name']; ?>"
                });

                marker.setMap(map);

            }

            google.maps.event.addDomListener(window, 'load', initialize);
        </script>

    </body>

</html>














