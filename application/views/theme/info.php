<div class="mega-call-us">
    <div class="container">
        <div class="call-mega-us">
            <i class="fa fa-phone-square"></i>
            <p>Call Us: <?php  echo $venues['contacts']; ?></p>
        </div>
        <div class="open-now text-right">
            <i class="fa fa-check-square"></i>
            <p>We are open now (<?php echo $venues['opening_time'] . ' - ' . $venues['closing_time']; ?>)</p>
        </div>
    </div>
</div>  