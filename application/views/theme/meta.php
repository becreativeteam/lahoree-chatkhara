
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title><?php echo $title . ' | ' . $this->config->item('site_title') ?></title>
<!-- Stylesheets -->
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="<?php echo base_url() . 'assets/' ?>css/bootstrap.css">
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<link rel="stylesheet" href="<?php echo base_url() . 'assets/' ?>css/style.css">
<link rel="stylesheet" href="<?php echo base_url() . 'assets/' ?>css/responsive.css">
<link rel="stylesheet" href="<?php echo base_url() . 'assets/' ?>css/owl.theme.css">
<link rel="stylesheet" href="<?php echo base_url() . 'assets/' ?>css/owl.carousel.css">
<link rel="stylesheet" href="<?php echo base_url() . 'assets/' ?>css/custom.css">

<!--[if IE 9]>
<script src="<?php echo base_url() . 'assets/' ?>js/media.match.min.js"></script>
<![endif]-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<!-- Local jQuery -->
<script type="text/javascript"> 
	var add_markar_object = {'position': '<?php echo $venues['latitude']; ?> , <?php echo $venues['longitude']; ?> ', 'bounds': true};
	var content_object = {'content': '<?php echo $venues['name']; ?>'};
</script>	