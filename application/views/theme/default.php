<!DOCTYPE html>
<html lang="en">
    <head>
        <?php echo $this->load->view('theme/meta'); ?>
        <script type="text/javascript">
          var base_url = "<?php echo base_url();?>"
        </script>
    </head> 

    <body>
        <div id="main-wrapper">  
            <?php echo $this->load->view('theme/header'); ?> 
            <div id="page-content">  
                <?php echo $this->load->view('theme/info'); ?> 
                <div class="container">
                    <div class="row">
                        <div class="col-md-7 col-md-push-3">
                            <?php echo $this->load->view($view); ?>
                        </div>
                        <div  class="col-md-3 col-sm-12 col-xs-12 col-md-pull-7">
                            <?php echo $this->load->view('theme/sidebar'); ?> 
                        </div>
                        <!--end .col-md-3 -->
                        <div class="col-md-2">
                            <?php echo $this->load->view('theme/your_order'); ?> 
                           
                            <!-- end .chekout class -->
                        </div>
                    </div>
                    <!-- end .row -->
                </div>   
                <?php echo $this->load->view('theme/footer'); ?>    
            </div> <!-- end .page-content -->
        </div>
        <!-- end #main-wrapper -->
        <?php echo $this->load->view('theme/footer_js'); ?>   
    </body>

</html>

