<div class="my-check-right">
    <h5 class="toggle-title">Your Order</h5>
    <ul id="cart_item" class="list-unstyled">
        <?php $price_total = 0.0;  ?> 
        <?php if ($cart_detail) { ?>
            <?php foreach ($cart_detail as $key => $item) { ?> 
                <li id="checkout_item_<?php echo $item->id; ?>" >
                    <p><?php echo $item->name; ?> <?php if(!empty($item->order_item)) {  ?>
                    (<?php echo  $item->order_item; ?>)
					<?php }  ?>
					</p>
					<?php 
					if(!empty($item->extra)) {  ?>
					 <p style="color:#000"> <?php echo $item->extra; ?> </p>
					<?php }  ?>
                    <p class="price"><span class="qty"><?php echo $item->qty; ?></span> x   <?php echo number_format($item->price,2); ?>   <span class="icon-link"><i class="fa fa-times remove_item"  data-itemid="<?php echo $item->id; ?>" data-itemtype="<?php echo $item->order_item; ?>"></i></span></p>
                </li>
                <?php $price_total += $item->total_price; ?>
            <?php } ?>
        <?php } else { ?>
            <li id="no-items"> <p>No Items</p> </li>    
        <?php } ?>       

    </ul>
    <ul class="list-unstyled">
        <li>  <!-- list for total price-->
            <p>Total</p>
            <p id="priceTotal" class="price-total"> <?php echo number_format($price_total,2); ?></p>
            <input type="hidden" name="" id="price_total" value="<?php echo number_format($price_total,2); ?>">
        </li>
    </ul>
    <div class="checkout">
        <a class="btn btn-default-red" href="<?php  echo base_url(); ?>takeaway/checkout"><i class="fa fa-shopping-cart"></i>Checkout</a> 
    </div>

</div>
