<header id="header">
    <div class="header-top-bar">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-5 col-xs-12 top-padding">
                    <div class="header-login">
                        <?php
                        $logged_user = $this->session->userdata('logged_user');
                        //print_r($logged_user); exit;
                        if ($logged_user['is_logedin'] == true) {
                            ?>
                            <a   href="#modal-profile" data-toggle="modal" > <?php echo "Welcome " . $logged_user['name']; ?> </a>
                            <?php
                        } else {
                            echo anchor('user/register', 'Register');
                        }
                        ?>



                        <?php
                        if ($logged_user['is_logedin'] == true) {
                            echo anchor('user/logout', 'Logout');
                        } else {
                            ?> 
                            <a href="#modal-login" data-toggle="modal"> Login </a>
                        <?php } ?>
                    </div>
                    <!-- end .header-login -->
                    <!-- Header Social -->
                    <ul class="header-social">
                        <li><a href="<?php //echo $venues['facebook'];               ?>" target="_blank"><i class="fa fa-facebook-square"></i></a> </li>
                        <li><a href="<?php //echo $venues['twitter'];               ?>" target="_blank"><i class="fa fa-twitter-square"></i></a> </li>                                  
                    </ul>
                </div>
                <div class="col-md-4 col-sm-2 col-xs-12">
                    <a class="navbar-brand" href="<?php echo base_url() ?>">
                        <img src="<?php echo base_url() ?>assets/img/header-logo.png" alt="<?php echo $venues['name']; ?>">
                    </a>
                </div>
            </div> <!-- end .row --> 
        </div> <!-- end .container -->
    </div>
    <!-- end .header-top-bar -->
    <div class="header-nav-bar">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li class="dropdown">
                            <a href="<?php echo base_url() ?>">Home</a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('takeaway/menu') ?>">Menu</a>
                        </li>
                        <!-- <li><a href="<?php // echo site_url('takeaway/checkout')        ?>">Checkout</a> -->
                        <li>
                            <a href="#<?php echo site_url('takeaway/checkout')        ?>">Checkout</a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('takeaway/opening_times') ?>">Opening Times</a> 
                        </li>

                        <li>
                            <a href="<?php echo site_url('takeaway/book_a_table') ?>">Book Table</a>
                        </li>

                        <li>
                            <a href="<?php echo site_url('takeaway/contact') ?>">Contact us</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>
    </div>
    <!-- end .header-nav-bar -->

    <div class="small-menu">
        <div class="container">
            <div class="choose-option">
                <ul class="list-unstyled list-inline">
                    <li class="<?php
                    if ($this->uri->segment(2) == "menu") {
                        echo "active";
                    }
                    ?>"><a href="#">1. Choose</a></li>

                    <li class="<?php
                    if ($this->uri->segment(2) == "checkout" and $this->session->userdata('is_logedin') == false) {
                        echo "active";
                    }
                    ?>"><a href="#">2. Checkout </a></li>
                    <li class="<?php
                    if ($this->uri->segment(2) == "checkout" and $this->session->userdata('is_logedin') == true) {
                        echo "active";
                    }
                    ?>" ><a href="#">2. Confirm </a></li>
                </ul>
            </div>
            <!-- end .choose-option-->
            <ul class="list-unstyled list-inline">
                <li><a href="<?php echo base_url() ?>">Home</a>
                </li>
                <li><i class="fa fa-chevron-right"></i>
                </li>
                <li><a href="<?php echo site_url('takeaway/menu') ?>">Menu</a>
                </li>
            </ul>
        </div>
        <!-- end .container-->
    </div>
    <!--end .small-menu -->
</header>
