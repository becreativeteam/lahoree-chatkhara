
<script>
    window.jQuery || document.write('<script src="<?php echo base_url() . '/assets/' ?>js/jquery-1.11.0.min.js"><\/script>')
</script>
<script src="<?php echo base_url() . 'assets/' ?>js/jquery-ui-1.10.4.custom.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() . 'assets/' ?>js/jquery.magnific-popup.min.js"></script>
<script src="<?php echo base_url() . 'assets/' ?>js/owl.carousel.js"></script>
<script src="<?php echo base_url() . 'assets/' ?>js/bootstrap.js"></script>
<script type="text/javascript" src="<?php echo base_url() . 'assets/' ?>js/jquery.ui.map.js"></script>
<script src="<?php echo base_url() . 'assets/' ?>js/scripts.js"></script>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDY0kkJiTPVd2U7aTOAwhc9ySH6oHxOIYM&sensor=false"> </script>
       <script>
var myCenter=new google.maps.LatLng(<?php echo $venues['latitude']; ?>,<?php echo $venues['longitude']; ?>);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:17,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  title:"<?php echo $venues['name']; ?>"
  });

marker.setMap(map);

}

google.maps.event.addDomListener(window, 'load', initialize);
</script>


