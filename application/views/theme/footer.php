<!-- footer begin -->
<footer id="footer">
    <div class="container">
        <div class="main-footer">
            <div class="col-md-7 col-sm-12">
                <div class="col-md-6 col-sm-6 border-right-bottom">
                    <h4>Head Office</h4>
                    <p>32-C, Shop# 9-10, Model Town, Link Road,Near Bata Shop, Lahore.</p>
                    <p>Ph: 042-35925769, 042-35925778</p>
                    <i class="fa fa-facebook-square"></i><p>/lahoreechatkhara</p>
                </div>
                <div class="col-md-6 col-sm-6 border-bottom">
                    <h4>Blue area, Islamabad</h4>
                    <p>No. 8, Ground Floor, Asif Plaza, 106 E, Fazal-e-Haq Road, Blue Area, Islamabad.</p>
                    <p>Ph: 051-2348558-59</p>
                    <i class="fa fa-facebook-square"></i><p>/lahoreechatkharabluearea</p>
                </div>
                <div class="col-md-7 col-sm-7">
                    <h4>Bahria Town, Islamabad</h4>
                    <p>Shop# 4-5, Business Bay, DHA Bahria Town,Islamabad.</p>
                    <p>Ph: 051-5400194-96</p>
                    <i class="fa fa-facebook-square"></i><p>/lahoreechatkharaphase7bahriaislamabad</p>
                </div>
            </div>
            <div class="col-md-5 col-sm-12 text-center franchise">
                <h1>Franchise Opportunity</h1>
                <h3>Available In All Major Cities</h3>
                <h5>FOR MORE INFORMATION</h5>
                <h2>0301 8540081</h2>
            </div>
        </div>
    </div>

    <div class="footer-copyright">
        <div class="container">
            <p>Copyright 2015 © <?php echo $venues['name']; ?>. All rights reserved. Powered by <a target="_blank" href="http://becreativedesign.co.uk/">Be Creative Design</a>.</p>
            <ul class="footer-social">
                <li><a target="_blank"  href="<?php //echo $venues['facebook'];      ?>"><i class="fa fa-facebook-square"></i></a>
                </li>
                <li><a target="_blank" href="<?php // echo $venues['twitter'];      ?>"><i class="fa fa-twitter-square"></i></a>
                </li>
            </ul>
            <!-- end .footer-social -->
        </div>
    </div>
</footer>

<!-- end #footer -->
<div class="modal fade" id="modal-login">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal heading -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h3 id="menu_event"  class="modal-title">Please Login </h3>
            </div>
            <!-- // Modal heading END -->
            <!-- Modal body -->
            <div class="modal-body">
                <div class="innerAll">
                    <div class="innerLR">
                        <form class="form-horizontal" action="<?php echo base_url(); ?>takeaway/login_user"  method="post" role="form">
                            <div class="form-group">

                                <div class="col-sm-12">
                                    <div style="color:red" id="login_response"> </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label"> Email </label>
                                <div class="col-sm-7">
                                    <input type="email" id="user_email" name="email" required  class="form-control" id="inputEmail3" placeholder="Email">
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-2 control-label"> Password </label>
                                <div class="col-sm-7">
                                    <input type="password" id="user_password" name="password" required  class="form-control" id="inputEmail3" placeholder="Password">
                                </div>
                            </div>


                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button  type="submit" class="btn btn-primary">Login</button>
                                    <a  class="btn btn-primary"  onclick="register();"  href="<?php echo site_url('user/register'); ?>"  > Register </a>
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>

                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- // Modal body END -->
        </div>
    </div>
</div>
<!-- // Modal Login END -->



<div class="modal fade bs-example-modal-sm" tabindex="-1" id="order-now" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title"> Welcome to our new website..</h4>
            </div>
            <div class="modal-body">
                <p>we are currently making some upgrades to the site. <strong>Call us to order on 01229 812 940!</strong> Soon you'll be able to order straight from our website!</p>
            </div>
        </div>
    </div>
</div>

