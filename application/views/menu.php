<div class="all-menu-details">
    <h5>Menu Items</h5>
    <div class="view-style dsn">
        <div class="list-grid-view">
        </div>
    </div>
    <?php foreach ($menu_items as $item) { ?>
        <div class="item-list">
            <div class="list-image">
                <img src="<?php echo $item['image_s'] ?>" id="image_<?php echo $item['menu_item_id'] ?>" alt="<?php echo $item['menu_item_name']; ?>">
            </div>
            <div class="all-details">
                <div class="visible-option">
                    <div class="details">
                        <h6 ><a href="#" id="name_<?php echo $item['menu_item_id']; ?>" data-name="<?php echo $item['menu_item_name']; ?>"><?php echo $item['menu_item_name'] ?></a> </h6>
                        <p class="for-list"  id="description_<?php echo $item['menu_item_id']; ?>" data-description="<?php echo $item['description']; ?>"><?php echo $item['description'] ?></p>
                        <p class="m-with-details"><?php echo $item['description'] ?></p>
                    </div>
                    <?php
                    $cart = $this->session->userdata('cart');
                    if ($cart) {
                        $cart_array = array();
                        foreach ($cart as $c) {
                            if (!empty($c['menu_item_id'])) {
                                $cart_array[] = $c['menu_item_id'];
                            }
                        }
                        $cart_array = implode(',', $cart_array);
                    } else {
                        $cart_array[] = '';
                    }
                    ?>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            // Confirm Delete.
                            $(".p_<?php echo $item['menu_item_id']; ?>").click(function() {
                                var o_items = $(this).val();
                                var ot = o_items.split(',');
                                $("#price_<?php echo $item['menu_item_id']; ?>").val(ot[0]);
                                $("#order_item_<?php echo $item['menu_item_id']; ?>").val(ot[1]);
                                $("#price_id_<?php echo $item['menu_item_id']; ?>").val(ot[2]);
                            });
                        });
                    </script>

                    <div class="price-option fl">
                        <?php  foreach ($item['prices'] as $p) { ?>
                            <h6 style='padding-left:13px; margin-top:-3px;'> <?php echo "<small>" . $p['title'] . "</small>"; ?>

                                <input type="radio" <?php
                                if ($p['default_price'] == true) {
                                    echo "checked";
                                } else { 
									 echo "checked";
								}
								
                                ?> name="price_<?php echo $item['menu_item_id']; ?>" class="p_<?php echo $item['menu_item_id']; ?>" value="<?php echo $p['price'] . "," . $p['title'] . "," . $p['price_id']; ?>" > <strong><?php echo number_format($p['price'], 2); ?></strong>


                                <?php
                                if ($p['default_price'] == true) {
                                    $default_price = $p['price'];
                                    $order_item = $p['title'];
                                    $price_id = $p['price_id'];
                                } else {
                                    $default_price = $p['price'];
                                    $order_item = $p['title'];
                                    $price_id = $p['price_id'];
                                }
                                ?>
                            </h6>

                        <?php } ?>
                        <input type="hidden" id="price_<?php echo $item['menu_item_id']; ?>" value="<?php
                           if ($default_price) {
                               echo $default_price;
                           }
                        ?>" />
                        <input type="hidden" id="price_id_<?php echo $item['menu_item_id']; ?>" value="<?php
                               if ($price_id) {
                                   echo $price_id;
                               }
                               ?>" />
                        <input type="hidden" id="order_item_<?php echo $item['menu_item_id']; ?>" value="<?php
                               if ($order_item) {
                                   echo $order_item;
                               }
                               ?>" />
                        <input type="hidden" id="featured_<?php echo $item['menu_item_id']; ?>" value="<?php // echo $item['featured']   ?>" />
                        <input type="hidden" id="serving_<?php echo $item['menu_item_id']; ?>" value="<?php echo $item['serving_size'] ?>" />
                        <input type="hidden" id="cart_items" value="<?php //echo $cart_array;   ?>" />
                        <!-- <button class="toggle">Option</button> -->
                    </div>
                    <!-- end .price-option-->
                    <div class="qty-cart text-center clearfix">
                        <h6>Qty</h6>
                        <form class="">
                            <input type="number" id="qty_<?php echo $item['menu_item_id'] ?>" name="qty_<?php echo $item['menu_item_id'] ?>" value="1" min="0">

                            <button class="add_to_cart" data-id="<?php echo $item['menu_item_id']; ?>"><i class="fa fa-shopping-cart"></i></button>
                        </form>
                    </div> <!-- end .qty-cart -->
                </div> <!-- end .vsible-option -->

                <!--end .dropdown-option-->
            </div>
            <!-- end .all-details -->
        </div>
<?php } ?>
    <!-- end .item-list -->
</div>




