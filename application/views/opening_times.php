<div class="page-content">

    <div class="container">



        <div class="row">
            <div class="col-md-8">

                <h6>Opening Times</h6>
                <ul class="nav nav-tabs mt30" role="tablist">
                    <li class="active"><a href="#tab-1" role="tab" data-toggle="tab">Opening Hours</a>
                    </li>
                    <li><a href="#tab-2" role="tab" data-toggle="tab">Timings</a>
                    </li>

                </ul>

                <div class="tab-content">
                    <div class="tab-pane fade in active" id="tab-1">
                        <table width="100%"> 
                            <thead>
                                <tr>
                                    <th width="20%"> Day </th>
                                    <th width="30%"> Lunch </th>
                                    <th width="50%"> Dinner </th>
                                </tr> 
                            </thead>
                            <tbody>
                                <tr>
                                    <td> Monday </td>
                                    <td> Closed </td>
                                    <td> Closed </td>
                                </tr>

                                <tr>
                                    <td> Tuseday </td>
                                    <td> Closed </td>
                                    <td> Thai Twosome/ A La Carte </td>
                                </tr>

                                <tr>
                                    <td> Wednesday </td>
                                    <td> Closed </td>
                                    <td> Early Bird/ A La Carte </td>
                                </tr>

                                <tr>
                                    <td> Thursday </td>
                                    <td> Closed </td>
                                    <td> Thai Twosome/Buffet/ A La Carte </td>
                                </tr>

                                <tr>
                                    <td> Friday </td>
                                    <td> Lunch Special </td>
                                    <td> Thai Twosome/Early Bird/ A La Carte </td>
                                </tr>

                                <tr>
                                    <td> Satuarday </td>
                                    <td> Lunch Special </td>
                                    <td> Thai Twosome/Early Bird/ A La Carte </td>
                                </tr>

                                <tr>
                                    <td> Sunday </td>
                                    <td> Closed </td>
                                    <td> Thai Twosome/Buffet/ A La Carte </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="tab-2">
                        <table width="100%"> 
                            <tbody>
                                <tr>
                                    <td> <strong> Lunch Buffet/ Lunch Special : </strong> </td>
                                    <td>  1200hrs - 1430hrs  </td>	
                                </tr>

                                <tr>
                                    <td> <strong> Evening Buffet/ Early Bird :  </strong> </td>
                                    <td>  1700hrs - 1830hrs  </td>	
                                </tr>

                                <tr>
                                    <td> <strong> Last Orders- Offers  : </strong> </td>
                                    <td>  1400hrs - 1800hrs  </td>	
                                </tr>

                                <tr>
                                    <td> <strong>  Last Orders- Thai Twosome  :  </strong> </td>
                                    <td>  2200hrs   </td>	
                                </tr>
                                <tr>
                                    <td> <strong> Last Orders- A La Carte  :  </strong> </td>
                                    <td> 1400hrs & 2200hrs  </td>	
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!-- end .container -->

</div>
<!-- end page-content -->
<br />