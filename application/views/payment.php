	
<div class="page-content">
      <!-- end .map-section -->
      <div class="contact-us">
        <div class="container">
          <div class="row">
            
            <div class="col-md-10">
              <div class="send-message">
                <h6 style="color:red"><?php echo $this->session->flashdata('msg'); ?></h6>
                <h4>Confirm your dilevery detail</h4>
                <form method="post">
                  <div class="row">
				  <div class="col-md-12 col-sm-6">
					<div style="color:red" id="response"> </div>
				  </div>
					<div class="col-md-5 col-sm-12">
                      Choose to pick up from Store 
                    </div>
					<div class="col-md-1 col-sm-12">
                      <input type="checkbox" id="store_address"  name="store_address" >
                    </div>
					
					<div class="col-md-7 col-sm-6">
                     Name  <input type="text"  id="full_name" value="<?php if(!empty($customer['first_name'])) { echo $customer['first_name'] . " " .  $customer['last_name']; }  ?>" name="full_name" placeholder="Full Name*">
                    </div>
					
                   
                    <div class="col-md-7 col-sm-6">
                      Mobile Phone <input type="text" id="phone" value="<?php if(!empty($customer['mobile_number'])) { echo $customer['mobile_number']; }  ?>"  name="phone" placeholder="Mobile Number*">
                    </div>
					
					<div class="col-md-7 col-sm-12">
                      Address <input type="text" id="address" value="<?php if(!empty($customer['address'])) { echo $customer['address']; }  ?>"   name="address" placeholder="Address*">
                    </div>
					
					<div class="col-md-7 col-sm-12">
                     City or Town <input type="text" id="city"   name="city" placeholder="Town or City">
                    </div>
					
					<div class="col-md-7 col-sm-6">
                      Postcode <input type="text" id="postcode"  name="postcode" placeholder="Postcode*">
                    </div>
					
					<div class="col-md-7 col-sm-12">
                      Delivery Time  <input type="text" id="d_time" value="<?php echo  date("h:i");?>"   name="d_time" placeholder="Delivery Time">
                    </div>
					
					<div class="col-md-10 col-sm-12">
                       Note <input type="text" id="note"  name="note" placeholder="Note">
                    </div>
					
					
					
                  </div>
                  <!-- end nasted .row -->
				   <button class="confirm_address" id="confirm_address" data-id=""><i class="fa fa-user"></i> Go to payment </button>
               </form>
              </div>
              <!-- end .send-message -->
            </div>
            <!-- end .main-grid-layout -->
		
          </div>
          <!-- end .row -->
        </div>
        <!-- end .container -->
      </div>
      <!-- end .contact-us -->
    </div>
    <!-- end page-content -->