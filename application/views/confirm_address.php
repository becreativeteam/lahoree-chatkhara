<script type="text/javascript"> 
$(function() {
        $("#customer_address").on("change", function()
        {
            var value_id = $("#customer_address").val();
			if(value_id == 0) { 
				$("#addressLine1").val('');
				$("#addressLine2").val('');
				$("#city").val('');
				$("#postcode").val('');
				$("#address_title").show(1);
				return false;
			}
            var form_data = {
                id: value_id
            };
            $.ajax({
                type: 'POST',
                url: '<?php echo site_url('takeaway/get_address'); ?>',
                data: form_data,
                success: function(msg) {
						$("#address_title").hide(1);
						var obj = $.parseJSON(msg);
						$("#addressLine1").val(obj['addressLine1']);
						$("#addressLine2").val(obj['addressLine2']);
						$("#city").val(obj['city']);
						$("#postcode").val(obj['postal_code']);
                }
            });
            $("#myModal").modal('show');
        });
    });
</script>

<div class="page-content">
    <!-- end .map-section -->
    <div class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-md-8">
                    <div class="send-message">
                        <h6 style="color:red"><?php echo $this->session->flashdata('msg'); ?></h6>
                        <h4>Confirm your dilevery detail</h4>
                        <?php 
						echo form_open('takeaway/confirm_address');?>
                            <div class="row">
							
                                <div class="col-md-12 col-sm-6">
                                    <div style="color:red" id="response"> </div>
                                </div>
                                 <div class="clearfix"></div>
                                <div class="col-md-5 col-sm-12">
                                    Name  <input type="text"  id="name" value="<?php if (!empty($customer['name'])) { echo $customer['name']; } ?>" name="full_name" placeholder="Full Name*">
                                    <input type="hidden"  id="customer_id" value="<?php if (!empty($customer['customer_id'])) { echo $customer['customer_id']; } ?>" name="id">
                                </div>


                                <div class="col-md-5 col-sm-12">
                                    Mobile Phone <input type="text" id="phone" value="<?php if (!empty($customer['mobile_number'])) { echo $customer['mobile_number'];} ?>"  name="phone" placeholder="Mobile Number*">
                                </div>
								
								<div class="col-md-10 col-sm-12">
									<select name="customer_address" id="customer_address">
									<option value="0"> +Add New Address </option>
                                    <?php  
									foreach($addresses as $address) {  ?>
									<option value="<?php echo $address['address_id']; ?>"> <?php echo $address['title']; ?> </option>
									<?php }?>
									
									</select>
                                </div>

                                <div class="col-md-10 col-sm-12" id="address_title">
                                    Address Title <input type="text" id="title" value=""   name="address" placeholder="Address Title">
                                </div>
								
								<div class="col-md-10 col-sm-12">
                                    Address 1 <input type="text" id="addressLine1" value=""   name="addressLine1" placeholder="Address 1">
                                </div>
								
								<div class="col-md-10 col-sm-12">
                                    Address 2 <input type="text" id="addressLine2" value=""   name="addressLine2" placeholder="Address 2">
                                </div>

                                <div class="col-md-4 col-sm-12">
                                    City or Town <input type="text" id="city" value=""   name="city" placeholder="Town or City">
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    Postcode <input type="text" id="postcode" value=""  name="postcode" placeholder="Postcode*">
                                </div>
							
								<div class="col-md-3 col-sm-6">
                                    Order Type  <br /> <select name="order_type" id="order_type"> 
													<option value="1"> Delivery </option>
													<option value="2"> Collection </option>
													<option value="3"> Reservation </option>
												</select>
                                </div>

                                <div class="col-md-7 col-sm-12">
                                    Delivery Time  <input type="text" id="d_time" value="<?php echo date("H:i A", strtotime('+50 minute')); ?>"   name="d_time" placeholder="Delivery Time">
                                </div>

                                <div class="col-md-10 col-sm-12">
                                    Note <input type="text" id="note"  name="note" placeholder="Note">
                                </div>
                            </div>
                            <!-- end nasted .row -->
                            <button  type="submit" class="confirm_address" id="confirm_address" ><i class="fa fa-user"></i> Go to payment </button>
                       <?php echo form_close();?>
                    </div>
                    <!-- end .send-message -->
                </div>
                <div class="col-md-4">
                    
                </div>
                <!-- end .main-grid-layout -->

            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </div>
    <!-- end .contact-us -->
</div>
<!-- end page-content -->
<script type="text/javascript">
    $(function() {
        $("#confirm_address").on('click', function() {
                var form_data = {
                    customer_id: $('#customer_id').val(),
                    address_id: $('#customer_address').val(),
                    name: $('#name').val(),
                    mobile: $("#phone").val(),
                    address_1: $("#addressLine1").val(),
                    address_2: $("#addressLine1").val(),
                    city: $("#city").val(),
					title: $("#title").val(),
					postcode: $("#postcode").val(),
                    country: $("#country").val(),
                    order_type: $("#order_type").val(),
                    d_time: $("#d_time").val(),
                    note: $("#note").val()
                };

            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('takeaway/confirm_address'); ?>",
                data: form_data,
                success: function(msg) {
                    window.location.replace("../takeaway/place_order");
                }
            });
            return false;
        });
    });
</script>