
<div class="tab-content">
    <div class="tab-pane fade in active" id="tab-1">
        <div class="all-menu-details">
            <h5>Checkout</h5>

            <?php if ($cart_detail) {
                $price_total = 0; ?>
    <?php foreach ($cart_detail as $key => $item) { ?> 
                    <div id="list_<?php echo $item->id; ?>"> 
                        <div class="item-list">
                            <div class="list-image">
                                <img src="<?php echo $item->image; ?>" id="image_<?php echo $item->id; ?>" alt="<?php echo $item->name; ?>">
                            </div>
                            <div class="all-details">
                                <div class="visible-option">
                                    <div class="details">
                                        <h6><a href="#"><?php echo $item->name; ?></a>
                                        </h6>

                <!-- <p class="for-list"> <?php // if(!empty( $item->option'])) {   ?> <?php // echo $item['option']. " | "; }   ?>   <?php //if(!empty( $item['extra'])) { foreach($item['extra'] as $key=>$ex) { echo $ex; } }   ?> </p> -->
                                        <p> <?php echo $item->description; ?> </p>
                                    </div>

                                    <div class="price-option fl">
                                        <h4><?php echo number_format($item->price * $item->qty, 2); ?></h4>
                                        <button class="toggle">Option</button>
                                    </div>
                                    <!-- end .price-option-->
                                    <div class="qty-cart text-center">
                                        <h6>Qty</h6>
                                        <form class="default-form">
                                            <input type="number" name="qty_<?php echo $item->id; ?>" data-id="<?php echo $item->id; ?>" class="quantity"  min="1" max="100" value="<?php echo $item->qty; ?>"  >
                                            <input type="hidden" id="total_price_<?php echo $item->id; ?>" value="<?php echo $item->price; ?>"  >
                                            <br>

                                        </form>
                                    </div> <!-- end .qty-cart -->
                                </div> <!-- end .vsible-option -->


                                <div class="dropdown-option clearfix">
                                    <div class="dropdown-details">
                                        <form class="default-form">
                                            <?php if (!empty($item->option) || !empty($item->extra)) { ?>
                                                <div class="clearfix"></div>
            <?php if (!empty($item->option)) { ?> 
                                                    <h6>Option</h6>
                                                    <div class="clearfix"></div>
                                                    <span class="">
                                                        <label for="option">
                <?php echo $item->option; ?>

                                                        </label>
                                                    </span>
                                                <?php } ?>

                                                <?php if (!empty($item->extra)) { ?> 
                                                    <h6>Extras</h6>
                <?php foreach ($item->extra as $k => $eList) { ?>
                                                        <span class="">
                                                            <label for="extra"><?php echo $eList; ?>
                                                            </label>
                                                        </span>
                                                    <?php }
                                                }
                                                ?>

                                            <?php } else { ?>
                                                <h5>There is no option available</h5>
                                                <div class="clearfix"></div>
        <?php } ?>
                                        </form>
                                    </div>
                                    <!--end .dropdown-details-->
                                </div>
                                <!--end .dropdown-option-->





                            </div>
                            <!-- end .all-details -->
                        </div>
                        <!-- end .item-list -->
                    </div>
        <?php $price_total += $item->price * $item->qty; ?>
    <?php } ?>

                <div class="item-list">
                    <div class="all-details">
                        <div class="visible-option">
                            <div class="details">
                                <h6>Total </h6>	
                                <?php if ($this->session->userdata('is_logedin') == true) { ?>
                                    <p> Customer Name : <?php echo $this->session->userdata('first_name'); ?></p>
                                    <p> Customer Email : <?php echo $this->session->userdata('email'); ?></p>
    <?php } ?>
                            </div>
                            <div class="price-option fl">
                                <h4> <?php echo number_format($price_total, 2); ?></h4>
                            </div>
                            <div class="text-center" >
                                <?php $logged_user = $this->session->userdata("logged_user");
                                if ($logged_user["is_logedin"] == true) {
                                    ?>
                                    <a class="btn btn-default-red" style="margin-top:20px"  href="<?php echo base_url(); ?>user/confirm_address" ><i class="fa fa-shopping-cart"></i>Confirm</a>
    <?php } else { ?>
                                    <a  class="btn btn-default-red" style="margin-top:20px"  href="#modal-login" data-toggle="modal" > Continue </a>
    <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end .item-list -->

            <?php } else { ?>
                <br> <br />
                <h4> No Record Found </h4>
<?php } ?>
        </div>
        <!--end all-menu-details-->
    </div> <!-- end .tab-pane  -->
</div> <!-- end .tab-content -->


<script type="text/javascript">
    $(function() {
        $(".quantity").on('click', function() { 
				var qty = $(this).val();
				var id = $(this).data("id");
					
                var form_data = {
                    qty: qty,
                    item_id: id
                };

            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('takeaway/change_qty'); ?>",
                data: form_data,
                success: function(msg) {
                    location.reload();
                }
            });
            return false;
        });
    });
</script>
