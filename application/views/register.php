<script type="text/javascript"> 
	$(function() {
		$("#register").on('click', function() {
			if ($('#email').val() == '' || $('#first_name').val() == '' || $('#last_name').val() == '' || $('#phone').val() == '' || $('#password').val() == '' || $('#address_1').val() == '' || $('#postcode').val() == '' || $('#city').val() == '') {
                $("#response").html("Required all fields!");
                return false;
            } else if($("#password").val() != $("#conf_password").val()) { 
				$("#response").html("Password do not match!");
                return false;
			} else if(document.getElementById("temrs_condition").checked == false) { 
				$("#response").html("Please Accept tems & conditions");
                return false;
			} else { 
				
			}
		});
	});
</script>

<div class="page-content">
      <!-- end .map-section -->
      <div class="contact-us">
        <div class="container">
          <div class="row">
            
            <div class="col-md-7">
              <div class="send-message">
                <h4>Register here for your Order</h4>
                <form action="<?php echo base_url(); ?>takeaway/register" method="post">
                  <div class="row">
					<div class="col-md-12 col-sm-6">
					<div style="color:red" > <?php echo $this->session->flashdata('reg_msg'); ?> </div>
				  </div>
				  <div class="col-md-12 col-sm-6">
					<div style="color:red" id="response"> </div>
				  </div>
					<div class="col-md-6 col-sm-6">
						<input type="email"  id="email" name = "email" placeholder="Email*">
                    </div>
					 <div class="col-md-6 col-sm-6">
                      <input type="text" id="phone"  name="phone" placeholder="Mobile Number*">
                    </div>
					<div class="col-md-6 col-sm-6">
                      <input type="password" id="password"  name="password" placeholder="Password*">
                    </div>
					
					<div class="col-md-6 col-sm-6">
                      <input type="password" id="conf_password"  name="conf_password" placeholder="Confirm Password*">
                    </div>
					
                    <div class="col-md-6 col-sm-6">
                      <input type="text" 	 id="first_name"  name="first_name" placeholder="First Name*">
                    </div>
					 <div class="col-md-6 col-sm-6">
                      <input type="text"  id="last_name"  name="last_name" placeholder="Last Name*">
                    </div>
                   
					<div class="col-md-10 col-sm-12">
                      <input type="text" id="address_1"  name="address_1" placeholder="Address1*">
                    </div>
					<div class="col-md-10 col-sm-12">
                      <input type="text" id="address_2"  name="address_2" placeholder="Address2*">
                    </div>
					<div class="col-md-4 col-sm-12">
                      <input type="text" id="city"  name="city" placeholder="Town or City">
                    </div>
					
					<div class="col-md-4 col-sm-6">
                      <input type="text" id="postcode"  name="postcode" placeholder="Postcode*">
                    </div>
					
					<div class="col-md-4 col-sm-12">
                      <input type="text" id="country"  name="country" placeholder="Country*">
                    </div>
					
					<div class="col-md-6 col-sm-12">
                      I accept the Baan Siam terms and conditions 
                    </div>
					
					<div class="col-md-1 col-sm-12">
                      <input type="checkbox" id="temrs_condition"  name="temrs_condition" >
                    </div>
					
                  </div>
                  <!-- end nasted .row -->
				   <button class="register" id="register" data-id=""><i class="fa fa-user"></i> Continue</button>
               </form>
              </div>
              <!-- end .send-message -->
            </div>
            <!-- end .main-grid-layout -->
			
			
			
			
			
			
			
			
			
			
			
			
			
			
			 <div class="col-md-5">
              <div class="send-message">
                <h4>Login</h4>
                <form action="<?php echo base_url(); ?>takeaway/login_user" method="post">
                  <div class="row">
				  <div class="col-md-12 col-sm-6">
					<div style="color:red" > <?php echo $this->session->flashdata('msg'); ?> </div>
				  </div>
					<div class="col-md-9 col-sm-6">
						<input type="email"  id="email" required name = "email" placeholder="Email*">
                    </div>
					<div class="col-md-9 col-sm-6">
                      <input type="password" id="password" required  name="password" placeholder="Password*">
                    </div>
                  </div>
                  <!-- end nasted .row -->
				   <button type="submit" data-id=""><i class="fa fa-user"></i> Login </button>
               </form>
              </div>
              <!-- end .send-message -->
            </div>
            <!-- end .main-grid-layout -->
		
          </div>
          <!-- end .row -->
        </div>
        <!-- end .container -->
      </div>
      <!-- end .contact-us -->
    </div>
    <!-- end page-content -->