<script type="text/javascript">

    $(function () {
        $("#submit_btn").on("click", function () {

            var name = $('#name').val();
            var email = $('#email').val();
            var phone = $('#phone').val();
            var message = $('#message').val();

            var form_data = {
                name: name,
                email: email,
                phone: phone,
                message: message
            };

            $.ajax({
                type: 'POST',
                url: '<?php echo site_url("takeaway/ajax_contact"); ?>',
                data: form_data,
                success: function (msg) {
                    $("#response").html(msg);
                }
            });
        });
        return false;
    });
</script>
<div class="page-content">

    <div class="map-section">
        <div id="map_canvas"></div>

    </div>
    <!-- end .map-section -->
    <div class="contact-us">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="contact-details">
                        <h4>Contact Details</h4>
                        <div class="row">
                            <div class="col-md-8 col-sm-6">
                                <h5><?php echo $venues['name']; ?></h5>
                                <div class="address clearfix">
                                    <p><i class="fa fa-map-marker"></i> </p>
                                    <p><?php echo str_replace(',', '<br />', $venues['address']); ?></p>
                                </div>
                                <div class="time-to-open clearfix">
                                    <p><i class="fa fa-clock-o"></i>
                                    </p>
                                    <p>
                                        <strong> Opening Time :  </strong> <?php echo $venues['opening_time']; ?> <br />
                                        <strong> Closing Time :  </strong> <?php echo $venues['closing_time']; ?>
                                    </p>
                                </div>

                                <div class="time-to-open clearfix">
                                    <p><i class="fa fa-clock-o"></i></p>
                                    <p>
                                        <strong> Cotnacts :  </strong> <?php echo $venues['contacts']; ?> <br /> 
                                       
                                    </p>
                                </div>
                            </div>

                            <!-- end .grid-layout -->
                        </div>
                        <!-- end nasted .row -->
                    </div>
                    <!-- end .contact-details -->
                </div>
                <!-- end .main-grid-layout -->

                <div class="col-md-6">
                    <div class="send-message">
                        <h4>Send Us a Message</h4>
                        <form action="#" onsubmit="return false" method="post">
                            <div class="row">
                                <div style="color:red" id="response"> </div>
                                <div class="col-md-12">
                                    <input type="text" required id="name"  name="name" placeholder="Name*">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="email" required id="email" name = "email" placeholder="Email*">
                                </div>
                                <div class="col-md-6 col-sm-6">
                                    <input type="text" id="phone" name="phone" placeholder="Phone">
                                </div>
                            </div>
                            <!-- end nasted .row -->
                            <textarea name='message' required id="message"  placeholder="Your message"></textarea>
                            <button class="submit_btn" id="submit_btn" onclick="return false;"><i class="fa fa-paper-plane-o"></i> Submit Message</button>
                        </form>
                    </div>
                    <!-- end .send-message -->
                </div>
                <!-- end .main-grid-layout -->
            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </div>
    <!-- end .contact-us -->
</div>
<!-- end page-content -->