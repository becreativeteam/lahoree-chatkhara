<div class="page-content">
    <!-- end .map-section -->
    <div class="contact-us">
        <div class="container">
            <div id="booking_message"> </div> 
            <div class="row" id="complete_form">
			
                <div class="col-md-10">
                    <div class="send-message">
                        <h4>Book a Table</h4>
                       <form action="<?php echo base_url(); ?>takeaway/booking_email" method="POST" id="booking_form"  enctype="multipart/form-data" >
                            <div class="row">
                                <div class="col-md-12 col-sm-6">
                                    <div style="color:red" id="response_1"> </div>
                                </div>
                                <div class="col-md-3 col-sm-6">
                                    <input type="text" name="bday" min="<?php echo date('d-m-Y'); ?>" id="datepicker" value="<?php echo date('d-m-Y'); ?>"  name="booking_date">
                                </div>

                                <div class="col-md-2 col-sm-6">
                                    <input type="number"  id="covers" min="1" max="12" placeholder="2 Guests"  name="covers" >
                                </div>
                                <div style="" class="col-md-2 col-sm-6">
                                    <select name="booking_type" id="booking_type">
                                        <option value="lunch"> Lunch </option>
                                        <option value="dinner"> Dinner </option>
                                    </select>
                                </div>

                                <div class="col-md-2 col-sm-6">
                                    <select  style=" width:80%" name="timing" id="timing"> 
                                        <option value='12:00:00'> 12:00 </option>
                                        <option value='12:15:00'> 12:15 </option>
                                        <option value='12:30:00'> 12:30 </option>
                                        <option value='12:45:00'> 12:45 </option> 
                                        <option value='13:00:00'> 13:00 </option>
                                        <option value='13:15:00'> 13:15 </option>
                                        <option value='13:30:00'> 13:30 </option>
                                        <option value='13:45:00'> 13:45 </option>
                                        <option value='14:00:00'> 14:00 </option>
                                        <option value='14:15:00'> 14:15 </option>
                                    </select>
                                </div>

                                <div class="col-md-3 col-sm-6">
                                    <button class="check_available" id="check_available" data-id=""><i class="fa fa-user"></i> Find Availabilty </button>
                                </div>
                            </div>

  </form>
                      
                    </div>
                    <!-- end .send-message -->
                </div>
                <!-- end .main-grid-layout -->

                <?php $logged_user = $this->session->userdata('logged_user'); ?>

                <div class="col-md-10" id="available_detail"> <br /> 
                    <div class="send-message">
                        <h4>Select Available Time</h4>
                        <form method="post">
                            <div class="row">
                                <div class="col-md-12 col-sm-6">
                                    <div style="color:red" id="response"> </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2 col-sm-6">
                                    <input type="text" id="name_title"  name="name_title" value="Mr" >
                                </div>

                                <div class="col-md-5 col-sm-6">
                                    <input type="text" id="first_name" value="<?php
                                    if (!empty($logged_user['first_name'])) {
                                        echo $logged_user['first_name'];
                                    }
                                    ?>"  name="first_name" placeholder="First Name">
                                </div>

                                <div class="col-md-5 col-sm-6">
                                    <input type="text" id="last_name"  name="last_name" placeholder="Second Name">
                                </div>
                            </div>
                            <div class="row">

                                <div class="col-md-5 col-sm-6">
                                    <input type="text" id="email" value="<?php
                                    if (!empty($logged_user['email'])) {
                                        echo $logged_user['email'];
                                    }
                                    ?>" placeholder="Email"  name="email" >
                                </div>
                                <div class="col-md-5 col-sm-12">
                                    <input type="text" id="mobile" value="<?php
                                    if (!empty($logged_user['mobile'])) {
                                        echo $logged_user['mobile'];
                                    }
                                    ?>"  name="mobile" placeholder="Phone Number">
                                </div>
                            </div>



                            <div class="row"> 
                                <div class="col-md-3 col-sm-6">
                                    <button class="book_table" id="book_table" data-id=""><i class="fa fa-user"></i> Book Table </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <!-- end .send-message -->
                </div>
                <!-- end .main-grid-layout -->


                <div class="col-md-10" id="confirm_booking"> <br />
                    <div class="send-message">
                        <h4>Confirm Order</h4>
                        <form method="post">
                            <div class="row">
                                <div class="col-md-5 col-sm-6">
                                    <div style="background-color:#FFF" id="customer_detail"> </div>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-md-12 col-sm-6">
                                    <u> <div style="color:blue; font-size:20px" id="book_date">  </div> </u>
                                </div>
                            </div>
                            <div class="row"> 
                                <div class="col-md-12 col-sm-6">
                                    <strong> Baan Siam 231 Dalton Rd, Barrow in Furnes, Cumbria LA14 1PQ <br />
                                        General Availabilty <br /> </strong>
                                    <div id="time_detail"> </div>
                                </div>
                            </div>

                            <div class="row"> 
                                <div class="col-md-3 col-sm-6">
                                    <button class="confirm_booking" id="confirm_booking" data-id=""><i class="fa fa-user"></i> Book Now </button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!-- end .send-message -->
                </div>
                <!-- end .main-grid-layout -->


            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </div>
    <!-- end .contact-us -->
</div>
<!-- end page-content -->

<script type="text/javascript">
    $(function () {
        if ($('#email').val() == "" || $('#first_name').val() == "" || $('#last_name').val() == "") {
            $("#available_detail").hide(1);
            $("#confirm_booking").hide(1);
        }

        $("#booking_type").on("change", function () {
            var booking_type = $("#booking_type").val();
            if (booking_type == "lunch") {
                $("#timing").html("<option value='12:00:00'> 12:00 </option> <option value='12:15:00'> 12:15 </option> <option value='12:30:00'> 12:30 </option> <option value='12:45:00'> 12:45 </option> <option value='13:00:00'> 13:00 </option><option value='13:15:00'> 13:15 </option><option value='13:30:00'> 13:30 </option><option value='13:45:00'> 13:45 </option><option value='14:00:00'> 14:00 </option><option value='14:15:00'> 14:15 </option>");
            } else {
                $("#timing").html("<option value='16:30:00'> 16:00 </option> <option value='16:45:00'> 16:45 </option> <option value='17:00:00'> 17:00 </option> <option value='17:15:00'> 17:15 </option> <option value='17:30:00'> 17:30 </option><option value='17:45:00'> 17:45 </option><option value='18:00:00'> 18:00 </option><option value='18:15:00'> 18:15 </option><option value='18:30:00'> 18:30 </option><option value='18:45:00'> 18:45 </option><option value='19:00:00'> 19:00 </option><option value='19:15:00'> 19:15 </option><option value='19:30:00'> 19:30 </option><option value='19:45:00'> 19:45 </option><option value='20:00:00'> 20:00 </option><option value='20:15:00'> 20:15 </option><option value='20:30:00'> 20:30 </option><option value='20:45:00'> 20:45 </option><option value='21:00:00'> 21:00 </option><option value='21:15:00'> 21:15 </option><option value='21:30:00'> 21:30 </option><option value='21:45:00'> 21:45 </option><option value='22:00:00'> 22:00 </option><option value='22:15:00'> 22:15 </option><option value='22:30:00'> 22:30 </option><option value='22:45:00'> 22:45 </option>");
            }

        });
        $("#check_available").on('click', function () {
            if ($('#covers').val() == '') {
                $("#response_1").html("Required all fields!");
                return false;
            } else {
                var form_data = {
                    "booking_date": $('#datepicker').val(),
                    "timing": $("#timing").val(),
                    "covers": $("#covers").val()
                };

                $.ajax({
                    type: 'POST',
                    url: "<?php echo $this->config->item('api_url') ?>booking/available_booking/venue_id/<?php echo $this->config->item('venue_id'); ?>",
                    data: form_data,
                    success: function (msg) {
                        if (msg['status'] == true) {
                            $("#response_1").html(msg['msg']);
                            $("#available_detail").show(1);
                        } else {
                            $("#response_1").html(msg['msg']);
                            $("#available_detail").hide(1);
                        }
                    }
                });
            }

            return false;
        });
		
        $("#book_table").on('click', function () {
            if ($('#first_name').val() == '' || $('#mobile').val() == '' || $('#email').val() == '' || $('#last_name').val() == "") {
                $("#response").html("Required all fields!");
            } else {
                $("#response").html("");
                var customer_detail = $('#first_name').val() + " " + $('#last_name').val();
                customer_detail += "<br />" + $('#email').val();
                customer_detail += "<br />" + $('#mobile').val();
                $("#customer_detail").html(customer_detail);
                $("#time_detail").html($('#covers').val() + " people at " + $('#timing').val());
                //var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
                //var month = ["Jan","Feb","March","April","May","June","July","Aug","Sep","Oct","Nov","Dec"];
                var book_date = $('#datepicker').val();
                $("#book_date").html(book_date);
                $("#confirm_booking").show(1);
            }

            return false;
        });


        $("#confirm_booking").on('click', function () {

            var form_data = {
                "booking_date": $('#datepicker').val(),
                "timing": $("#timing").val(),
                "covers": $("#covers").val(),
                "name_title": $("#name_title").val(),
                "name": $("#first_name").val() + " " + $("#last_name").val(),
                "mobile": $("#mobile").val(),
                "booking_type": $("#booking_type").val(),
                "email": $("#email").val()
            };
			
	
            $.ajax({
                type: 'POST',
                //url: "http://localhost/maduber/api/consumer/book_a_table/key/1410034567841811/venue_id/2",
				 url: "<?php echo $this->config->item('api_url') ?>booking/book_a_table/venue_id/<?php echo $this->config->item('venue_id'); ?>",
                data: form_data,
                success: function (msg) {
                    if (msg['status'] == true) {
                        $("#response").html("Thanks For your Booking Table!");
                        $("#booking_message").html("Thank you for Booking <br /> You Booking Detail : " + $('#covers').val() + " people at " + $('#datepicker').val() + " " + $('#timing').val());
                        $("#covers").val('');
                        $("#first_name").val('');
                        $("#last_name").val('');
                        $("#mobile").val('');
                        $("#email").val('');
                        $("#complete_form").hide(1);
						
								var counter = 2;
								var interval = setInterval(function() {
									counter--;
									if (counter == 0) {
										clearInterval(interval);
										 $.ajax(
										{
											url : "<?php echo base_url(); ?>takeaway/booking_email",
											type: "POST",
											data : form_data,
											success:function(data, textStatus, jqXHR) 
											{
												//data: return data from server
											},
											error: function(jqXHR, textStatus, errorThrown) 
											{
												//if fails      
											}
										});
									 }
									}, 1000);
									
						  
                    } else {
                        $("#response_2").html(msg['msg']);
                    }
                }
            });
            return false;
        });
    });


</script>
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

<script>
    $(function () {
        $("#datepicker").datepicker({
			minDate: - 0,
			maxDate: "+1M",
			dateFormat : "d-M-y"
		});
    });
</script>