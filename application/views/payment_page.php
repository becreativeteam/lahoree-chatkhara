<?php
$order_detail = $this->session->userdata('order_detail');
$logged_user = $this->session->userdata('logged_user');
?>
<script type="text/javascript">
    $(function () {
        $("#my_cards").on("change", function () {
			var card = $("#my_cards").val();
			if(card == 'new') { 
				$("#card_detail").show(1);
			} else { 
				$("#card_detail").hide(1);
			}
		 });
	});

    //order_completed();

</script>

<div class="page-content">
    <!-- end .map-section -->
    <div class="contact-us">
        <div class="container">
            <div class="row">

                <div class="col-md-6">
                    <div class="send-message">
					 <div style="color:red"> <?php echo $this->session->flashdata('error_msg'); ?> </div>
					 
                        <h4 style="color:red">How would you like to pay?</h4>
                       
                        <form action="<?php echo base_url(); ?>takeaway/verify_order" method="post">
								<div class="col-md-10 col-sm-12">
                                   <input type="radio" id="payment_method"  value="cash"  name="payment_method" > Pay with Cash
                                </div>
								<div class="col-md-10 col-sm-12">
                                   <input type="radio" id="payment_method" checked  value="card"  name="payment_method" > Pay with debit or credit card
                                </div>
						
					   <div class="row">
								<div class="col-md-6 col-sm-6">
                                    Select Card <br>  <select  id="my_cards"  name="my_cards"> 
										<option value="new"> Add New </option>
										<?php  foreach($customer_cards as $cc) {  ?>
											 <option value="<?php  echo $cc['credit_card_id']; ?>"> <?php  echo "**** **** **** ".$cc['last4']  ?>  </option>
										<?php  } ?>
                                    </select>
                                </div>
					   </div>
					   
						<div class="row"> 
							<div class="col-md-12 col-sm-6"> <h4> Credit Card Information </h4> </div>
						</div>
					   
					   <div class="row" id="card_detail">
                                <div class="col-md-6 col-sm-6">
                                    Card Type <br>  <select  id="cc_card"  name="cc_card"> 
                                        <option value="V"> Visa </option>
                                        <option value="M"> Mastercard </option>
                                        <option value="A"> American express </option>
                                        <option value="D"> Discover </option>
                                    </select>
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    Card Number 
                                </div>
                                <div class="col-md-7 col-sm-6">
                                    <input type="text"  id="card_number" value="" name="card_number" >
                                </div>

                                <div class="col-md-12">
                                    <input type="checkbox" name="save_card" id="save_card" >
                                    Save my card for quicker reordering 
                                </div>
                                <div class="col-md-12 col-sm-6">
                                    Expiry Date 
                                </div>
                                <div class="col-md-2 col-sm-6">
                                    <select name="cc_month" id="cc_month">
                                        <option value="01"> 01 </option>
                                        <option value="02"> 02 </option>
                                        <option value="03"> 03 </option>
                                        <option value="04"> 04 </option>
                                        <option value="05"> 05 </option>
                                        <option value="06"> 06 </option>
                                        <option value="07"> 07 </option>
                                        <option value="08"> 08 </option>
                                        <option value="09"> 09 </option>
                                        <option value="10"> 10 </option>
                                        <option value="11"> 11 </option>
                                        <option value="12"> 12 </option>
                                    </select>
                                </div>

                                <div class="col-md-4 col-sm-6">
                                    <select name="cc_year" id="cc_year">
                                        <?php
                                        $current_year = date('Y');
                                        for ($i = 0; $i < 5; $i++) {
                                            ?>
                                            <option value="<?php echo $current_year + $i; ?>"> <?php echo $current_year + $i; ?> </option>
<?php } ?>
                                    </select>
                                </div>
                                <br />

                                <div class="col-md-12 col-sm-6">
                                    Secuirty Number
                                </div>

                                <div class="col-md-3 col-sm-6">
                                    <input type="text"   id="cc_code" value="" name="cc_code" >
                                </div>
                                <div class="col-md-8 col-sm-6">
                                    Last 3 digits of the number of the back of your card
                                </div>
                                <br />
                                <br />
                                <div class="col-md-12 col-sm-6">
                                   <strong> Billing address : </strong> <?php echo $address_detail['addressLine1'] . ", " . $address_detail['city'] . ", " . $address_detail['postal_code']; ?>
                                </div>

                                <div class="col-md-12 col-sm-6">

                                </div>

                            </div>
							<br />
                            <button type="submit"  ><i class="fa fa-user"></i> Place my Order </button>
                        </form>



                    </div>
                    <!-- end .send-message -->
                </div>
                <!-- end .main-grid-layout -->

                <div class="col-md-5">
                    <div class="send-message">
                        <h4 align="center">Your Order</h4>
                        <table width="100%"> 
                            <?php
                            if ($cart_detail) {
                                $total_price = 0;
                                foreach ($cart_detail as $key => $item) {
                                    ?> 
                                    <tr> 
                                        <td width="70%"> <?php echo $item->name; ?> </td>
                                        <td>   <?php echo $item->qty . " * " . $item->price; ?> </td>
                                    </tr>
                                    <?php
                                    $total_price = $total_price + ($item->price * $item->qty);
                                }
                            }
                            ?>
                            <tr style="border-top:1px solid"> 
                                <th> TOTAL </th>
                                <td><?php echo number_format($total_price, 2); ?>&pound;</td>
                            </tr> 
                            <tr style="border-top:1px solid"> 
                                <td colspan="2"> <?php
                                    echo $order_detail['name'] . "<br />";
                                    echo $address_detail['addressLine1'] . "<br />";
                                    echo $order_detail['mobile_number'] . "<br />";
                                    ?> </td>
                            </tr>

                            <tr style="border-top:1px solid"> 
                                <td colspan="2"> Delivery Request for <u><?php echo $order_detail['d_time'] ?> </u></td>
                            </tr>
                        </table>

                    </div>
                    <!-- end .send-message -->
                </div>
                <!-- end .main-grid-layout -->

            </div>
            <!-- end .row -->
        </div>
        <!-- end .container -->
    </div>
    <!-- end .contact-us -->
</div>
<!-- end page-content -->