 <div class="latest-from-blog text-center">
                    <div class="container">
                        <h4>Latest Events</h4>
                        <div class="row">
                            <?php foreach ($events as $key => $event) { ?>
                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                                    <div class="blog-latest">
                                        <div class="row">
                                            <div class="col-md-6 col-sm-12">
                                                <img class="" src="<?php echo $event['image_m'] ?>" alt="<?php echo $event['title'] ?>">
                                            </div>
                                            <div class="col-md-6 col-sm-12">
                                                <h5><a href="#"><?php echo $event['title'] ?></a> </h5>
                                                <p><i class="fa fa-clock-o"></i>
                                                    <span class="date"><?php echo date('d.n.Y',  strtotime( $event['starting_date']));?></span>at
                                                    <span class="time"><?php echo date('H.i',  strtotime( $event['starting_date']));?></span>
                                                </p>
                                                <p class="bl-sort"><?php echo $event['description'] ?></p>
                                                <!--<a href="#" class="btn btn-default-red"><i class="fa fa-file-text-o"></i> Read  More</a>-->
                                            </div>
                                            <!--end .events-details-->
                                        </div>
                                        <!--end .row-->
                                    </div>
                                    <!--end .events-latest -->
                                </div>
                                <!--end grid layout-->
                            <?php } ?>
                        </div>
                        <!--end .row main-->
                    </div>
                    <!--end container-->
                </div>
                <!--end .latest-from-events-->
				<br />