<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Takeaway extends CI_Controller {

    public function __construct() {
        parent::__construct();
         $this->load->model('order_model');
//         
//        $this->output->set_header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
//        $this->output->set_header("Pragma: no-cache");
        
				
    }
    public function index() {
	
      $home_slides = json_decode(file_get_contents($this->config->item('api_url') . 'home/slider/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$home_slides['status']) {
            show_404();
        }
		
		$venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }
		
		$events = json_decode(file_get_contents($this->config->item('api_url') . 'home/events/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$events['status']) {
            show_404();
        }
		
		$testimonials = json_decode(file_get_contents($this->config->item('api_url') . 'home/testimonial/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$testimonials['status']) {
            show_404();
        }
		
		$homepage = json_decode(file_get_contents($this->config->item('api_url') . 'home/homepage/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$homepage['status']) {
            show_404();
        }

        $data = array(
            'title' => 'Home',
            'homepage' => $homepage['data'],
            'home_categories' => $homepage['homepage'],
            'venues' => $venue['data'],
            'slides' => $home_slides['data'],
            'testimonials' => $testimonials['data'],
            'events' => $events['data'],
        );
        $this->load->view('theme/home', $data);
    }
   
    public function menu($c_slug = '') {
     
        $categories = json_decode(file_get_contents($this->config->item('api_url') . 'categories/getall/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$categories['status']) {
            show_404();
        }
        if ($c_slug == '' || $c_slug == 'featured') {
            $memu = json_decode(file_get_contents($this->config->item('api_url') . "menu/featured_items/venue_id/" . $this->config->item('venue_id')), TRUE);
            if (!$memu['status']) {
                $memu = FALSE;
            } else {
                $memu = $memu['data'];
            }
        } else {
            $memu = json_decode(file_get_contents($this->config->item('api_url') . "menu/menu_by_category_slug/venue_id/" . $this->config->item('venue_id') . '/slug/' . $c_slug), TRUE);
            if (!$memu['status']) {
                $memu = FALSE;
            } else {
                $memu = $memu['data'];
            }
        }
		
		$venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }

        $data = array(
            'title' => 'Menu',
            'view' => 'menu',
            'venues' => $venue['data'],
            'categories' => $categories['data'],
            'menu_items' => $memu,
        );
        if ($this->session->userdata('order_id')) {
            $data['cart_detail'] = $this->order_model->get_order_details($this->session->userdata('order_id'));
        } else {
            $data['cart_detail'] = array();
        }
        $this->load->view('theme/default', $data);
    }

    public function add_to_cart() {


        $id = $this->input->post('id');
        $p_qty = $this->input->post('qty');
        $p_price = $this->input->post('total_price');


        if (!$this->session->userdata('order_id')) {
            $data = array(
                "name" => "new order"
            );
            $insert_id = $this->order_model->insert_order($data);
            $this->session->set_userdata('order_id', $insert_id);
        } else {
            $insert_id = $this->session->userdata('order_id');
        }
        $cart_data = $this->order_model->get_order_details($insert_id);
        $cart_items = array();
        if ($insert_id) {
            foreach ($cart_data as $key => $item) {
                $cart_items[] = $item->id;
            }
        }

        if ($post_data = $this->input->post(NULL, TRUE)) {
            $post_data['order_id'] = $insert_id;
            if (in_array($id, $cart_items)) {
                $order_D = $this->order_model->get_order_detail($id);
				if($order_D->order_item == $this->input->post('order_item')) { 
                $qty = $order_D->qty;
                $price = $order_D->total_price;
                $new_qty = $p_qty + $qty;
                $new_price = $p_price + $price;
                $update_data = array(
                    "qty" => $new_qty,
                    "total_price" => $new_price
                );

                $this->order_model->update_order_detail($id, $update_data);
				} else {
					$this->order_model->insert_order_detail($post_data);
				}
			} else {
				$this->order_model->insert_order_detail($post_data);
			}
		
		}

        echo json_encode($this->order_model->get_order_details($insert_id));
    }

    public function cart_items_json() {
        echo json_encode($this->session->userdata('cart'));
    }

    public function change_qty() {
        $qty_new = $this->input->post('qty');
        $item_id = $this->input->post('item_id');

        $order_D = $this->order_model->get_order_detail($item_id);
        $price = $order_D->price;


        $update_data = array(
            "qty" => $qty_new,
            "total_price" => $price * $qty_new
        );

        $this->order_model->update_order_detail($item_id, $update_data);
    }

    public function remove_item() {
        $this->order_model->delete_order_details($this->input->post('id') , $this->input->post('type'));
        echo json_encode($this->order_model->get_order_details($this->session->userdata('order_id')));
    }

    public function clear_cart() {
        echo '<pre>';
        print_r($this->session->userdata('cart'));
        echo '</pre>';
        $this->session->sess_destroy();
    }

    /** contact us controler 
      Developed By :  Arfan
     */
    public function contact() {
        $venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }

        $data = array(
            'title' => 'Contact Us',
            'view' => 'contact',
            'venues' => $venue['data']
        );
        $this->load->view('theme/contact_theme', $data);
    }

    public function opening_times() {
       $venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }
        $data = array(
            'title' => 'Opening Times',
            'view' => 'opening_times',
            'venues' => $venue['data']
        );
        $this->load->view('theme/contact_theme', $data);
    }

    public function checkout() {
        
		
		$venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }

        $data = array(
            'title' => 'Checkout',
            'view' => 'checkout',
            'venues' => $venue['data']
        );
        if ($this->session->userdata('order_id')) {
            $data['cart_detail'] = $this->order_model->get_order_details($this->session->userdata('order_id'));
        } else {
            $data['cart_detail'] = array();
        }
        $this->load->view('theme/contact_theme', $data);
    }

    public function events() {
        $menupage = json_decode(file_get_contents($this->config->item('api_url') . "consumer/menupage/key/" . $this->config->item('vendor_key') . '/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$menupage['status']) {
            show_404();
        }

        $homepage = json_decode(file_get_contents($this->config->item('api_url') . "consumer/homepage/key/" . $this->config->item('vendor_key') . '/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$homepage['status']) {
            show_404();
        }

        $data = array(
            'title' => 'Events',
            'view' => 'events',
            'venues' => $menupage['venues'],
            'events' => $homepage['events']
        );
        $this->load->view('theme/contact_theme', $data);
    }

    // book a table 

    public function book_a_table() {
        $venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }
		
        $data = array(
            'title' => 'Book a Table',
            'view' => 'book_a_table',
            'venues' => $venue['data']
        );
		
        $this->load->view('theme/contact_theme', $data);
    }

    public function booking_email() {
		$mag = "";
		$msg .= "Name : " . $this->input->post('name_title') . " " . $this->input->post('name');
		$msg .= "<br />Email : " . $this->input->post('email');
		$msg .= "<br />Phone : " . $this->input->post('mobile');
		$msg .= "<br />Total People : " . $this->input->post('covers');
		$msg .= "<br />Booking Date : " . $this->input->post('booking_date');
		$msg .= "<br />Booking Time : " . $this->input->post('timing');
		$msg .= "<br />Booking For : " . $this->input->post('booking_type');
		 
        $this->load->library('email');

   
        $config['protocol'] = 'sendmail';
        $config['mailpath'] = '/usr/sbin/sendmail';
        $config['charset'] = 'iso-8859-1';
        $config['wordwrap'] = TRUE;

        $this->email->initialize($config);
        $this->email->set_mailtype("html");
        $this->email->from('arfan67@gmail.com', 'Baan Saim');
        $this->email->to("arfan67@gmail.com");
        $this->email->subject("New Table Booking Request");
        $this->email->message($msg);
        if ($this->email->send()) {
            echo " Email Successfully Send! <br />";
        } else {	
            echo " Email Sending Failed! <br />";
        }
    }

    function confirm_address() {

        if ($this->input->post('customer_id')) {
            $logged_user = $this->session->userdata('logged_user');

            $customer = json_decode(file_get_contents($this->config->item('api_url') . 'customer/get/customer_id/' . $logged_user['customer_id']), TRUE);
            if (!$customer['status']) {
                show_404();
            }
			
			if($this->input->post('address_id') == 0) { 
				$address_data = array(
					"customer_id" => $this->input->post('customer_id'),
					"title" => $this->input->post('title'),
					"addressLine1" => $this->input->post('address_1'),
					"addressLine2" => $this->input->post('address_2'),
					"postal_code" => $this->input->post('postcode'),
					"city" => $this->input->post('city')
				);
				
				
				$this->load->library('rest', array(
					'server' => $this->config->item('api_url')
				));

				$customer_address = $this->rest->post('customer/customer_address/', $address_data);
				
				if($customer_address->status) { 
					$address_id = $customer_address->address_id;
				}
				
			} else { 
				$address_id = $this->input->post('address_id');
			}	
			
			
            $data = array(
               "customer_id" => $this->input->post('customer_id'),
                "name" => $this->input->post('name'),
                "mobile_number" => $this->input->post('mobile'),
                "address_id" => $address_id,
                "d_time" => $this->input->post('d_time'),
				"order_type" => $this->input->post('order_type'),
                "note" => $this->input->post('note')
            );
        }
		
        $this->session->set_userdata('order_detail', $data);
		 return TRUE;
    }
	
	
	
	public function get_address() {  
		$id = $this->input->post('id');
		$address = json_decode(file_get_contents($this->config->item('api_url') . 'customer/get_customer_address/address_id/' . $id), TRUE);
		echo json_encode($address['data']);
	}

    public function place_order() {	
        $logged_user = $this->session->userdata('logged_user');
        $order_detail = $this->session->userdata('order_detail');
        if ($logged_user['is_logedin'] == false) {
            redirect('takeaway/checkout');
        }

       $venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }
		
		$customer_cards = json_decode(file_get_contents($this->config->item('api_url') . "customer/credit_card/customer_id/" . $logged_user['customer_id']), TRUE);
        
		 if($customer_cards['status']) { 
			$customer_card =  $customer_cards['data'];
		} else { 
			$customer_card = array();
		} 
		
		$address_detail = json_decode(file_get_contents($this->config->item('api_url') . "customer/get_customer_address/address_id/" . $order_detail['address_id']), TRUE);
		
		 if($address_detail['status']) { 
			$address_detail =  $address_detail['data'];
		} else { 
			$address_detail = array();
		} 
		
        $data = array(
            'title' => 'Payment',
            'view' => 'payment_page',
            'venues' => $venue['data'],
            'customer_cards' => $customer_card,
            'address_detail' => $address_detail
        );

        if ($this->session->userdata('order_id')) {
            $data['cart_detail'] = $this->order_model->get_order_details($this->session->userdata('order_id'));
        } else {
            $data['cart_detail'] = array();
        }
        $this->load->view('theme/contact_theme', $data);
    }

    public function order_complete() {
          $venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }
	
		
		$order_status = json_decode(file_get_contents($this->config->item('api_url') . 'orders/get_order/order_id/' . $this->session->userdata('current_order')), TRUE);
		
	
        if (!$order_status['status']) {
            show_404();
        }
		
		$logged_user = $this->session->userdata('logged_user');
		$order_detail = $this->session->userdata('order_detail');
		
		$previous_orders = json_decode(file_get_contents($this->config->item('api_url') . 'orders/get_customer_orders/customer_id/' . $logged_user['is_logedin']), TRUE);
		
		if($previous_orders['status'] == true) { 
			$pre_orders = $previous_orders['data'];
		} else { 
			$pre_orders = false;
		}
		
		$address_detail = json_decode(file_get_contents($this->config->item('api_url') . "customer/get_customer_address/address_id/" . $order_detail['address_id']), TRUE);
        
		 if($address_detail['status']) { 
			$address_detail =  $address_detail['data'];
		} else { 
			$address_detail = array();
		} 
		
        $data = array(
            'title' => 'Order Completed ',
            'view' => 'order_complete',
            'venues' => $venue['data'],
            'previous_orders' => $pre_orders,
			'order_status' => $order_status['data'],
			'address_detail' => $address_detail
        );
        if ($this->session->userdata('order_id')) {
            $data['cart_detail'] = $this->order_model->get_order_details($this->session->userdata('order_id'));
        } else {
            $data['cart_detail'] = array();
        }
		
		//$this->session->unset_userdata('order_id');
        $this->load->view('theme/contact_theme', $data);
    }

   
    function thank_you() {
        $menupage = json_decode(file_get_contents($this->config->item('api_url') . "consumer/menupage/key/" . $this->config->item('vendor_key') . '/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$menupage['status']) {
            show_404();
        }

        $data = array(
            'title' => 'Thank You',
            'view' => 'thank_you',
            'venues' => $menupage['venues']
        );
        $this->load->view('theme/contact_theme', $data);
    }

   public function login_user() { 
		
		$this->load->library('rest', array(
            'server' => $this->config->item('api_url')
                //  'http_user' => 'admin',
                //'http_pass' => '1234',
                //'http_auth' => 'basic' // or 'digest'
        ));
      
       $data = array(
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'venue_id' => $this->config->item('venue_id')
	   );
	 

        $customer = $this->rest->post('customer/login/', $data);
		
		
		
		if($customer->status) { 
			 $customer_data = array(
				"customer_id" => $customer->data->customer_id,
				"email" => $customer->data->email,
				"name" => $customer->data->name,
				"mobile" => $customer->data->mobile,
				"is_logedin" => $customer->data->is_loggedin
			);
			$this->session->set_userdata('logged_user', $customer_data);
			redirect("takeaway/menu");
		} else { 
			$this->session->set_flashdata('msg' , $customer->msg);
			redirect("user/register");
		}
   }

   
    public function register() { 
		
		
		$this->load->library('rest', array(
            'server' => $this->config->item('api_url')
                //  'http_user' => 'admin',
                //'http_pass' => '1234',
                //'http_auth' => 'basic' // or 'digest'
        ));
	
       $data = array(
			'venue_id' => $this->config->item('venue_id'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'name' => $this->input->post('first_name') . " " . $this->input->post('last_name'),
			'mobile_number' => $this->input->post('phone'),
			"address_1" =>$this->input->post('address_1'),
			'address_2' => $this->input->post('address_2'),
			'city' => $this->input->post('city'),
			'postcode' => $this->input->post('postcode'),
			'country' => $this->input->post('country')
	   );
	   
		$customer = $this->rest->post('customer/register/', $data);
		
		$customer = $customer->data;

		$address = array();
		$address['customer_id'] = $customer->customer_id;
		$address['title'] =  "Address";
		$address['addressLine1'] =  $this->input->post('address_1');
		$address['addressLine2'] =  $this->input->post('address_2');
		$address['city'] =  $this->input->post('city');
		$address['postal_code'] =  $this->input->post('postcode');
		
	    $this->rest->post('customer/customer_address/', $address);
		 
		 
      //  $customer = $this->rest->post('consumer/register/key/1410034567841811/', $data);
		if($customer->status) { 
			 $customer_data = array(
				"customer_id" => $customer->customer_id,
				"email" => $this->input->post('email'),
				"first_name" => $this->input->post('first_name'),
				"mobile" =>$this->input->post('phone'),
				"address_1" =>$this->input->post('address_1'),
				'address_2' => $this->input->post('address_2'),
				'city' => $this->input->post('city'),
				'postcode' => $this->input->post('postcode'),
				'country' => $this->input->post('country'),
				"is_logedin" => 1
			);
			$this->session->set_userdata('logged_user', $customer_data);
			$cart_detail = $this->order_model->get_order_details($this->session->userdata('order_id'));
			if($cart_detail) { 
				redirect("takeaway/checkout");
			} else { 
				redirect("takeaway/menu");
			}	
		} else { 
			$this->session->set_flashdata('reg_msg' , $customer->error);
			redirect("user/register");
		}
   }
   
    public function verify_order() {
		if($this->input->post('payment_method')) { 
			$payment_method = $this->input->post('payment_method');
		} 
		if($payment_method == "card") { 
			$this->load->library('rest', array(
            'server' => $this->config->item('api_url'),
                //  'http_user' => 'admin',
                //'http_pass' => '1234',
                //'http_auth' => 'basic' // or 'digest'
			));
	
			
			if($this->input->post('my_cards') != "new") { 
				$this->order_completed('card' , $this->input->post('my_cards'));
				
			} else { 
				$logged_user = $this->session->userdata('logged_user');
				$cc_type = $this->input->post('cc_type');
				$cc_number = $this->input->post('card_number');
				$cc_year = $this->input->post('cc_year');
				$cc_month = $this->input->post('cc_month');
				$cc_code = $this->input->post('cc_code');
				$data = array(
					"number" => $this->input->post('card_number'),
					"exp_month" => $this->input->post('cc_month'),
					"exp_year" => $this->input->post('cc_year'),
					"cvc" => $this->input->post('cc_code'),
					"name" => $this->input->post('cc_card')
				);
				 
				$this->load->library('stripe');
				$stripe_data =  $this->stripe->createTokenWithCard($data);
				$data_json = json_decode($stripe_data);
				
					if(!empty($data_json->id)) { 
						$s_data = array(
							'token_id' => $data_json->id,
							'last4' => $data_json->card->last4,
							'brand' => $data_json->card->brand,
							'customer_id' => $logged_user['customer_id'],
							'address_1' =>$this->input->post('address_1'),
							'address_2' => $this->input->post('address_2'),
							'city' => $this->input->post('city'),
							'postcode' => $this->input->post('postcode'),
							'country' => $this->input->post('country')
						);
						
					} else { 
						$this->session->set_flashdata('error_msg' , $data_json->message);
						redirect('takeaway/place_order');
					}
					
				
				$add_card = $this->rest->post('customer/add_credit_card/', $s_data);
				
					
					
				if($add_card->status) { 
					
					$this->order_completed('card' , $add_card->credit_card_id);
				} else { 
					$this->session->set_flashdata('error_msg' , $add_card->msg);
					redirect('takeaway/place_order');
				}
			}
		} else { 
				$this->order_completed('cash' , '');
		}
	}
	
	function order_completed($payment_method , $credit_card_id = '') { 
		//echo $payment_method . $credit_card_id,  exit;
		$this->load->library('rest', array(
            'server' => $this->config->item('api_url'),
                //  'http_user' => 'admin',
                //'http_pass' => '1234',
                //'http_auth' => 'basic' // or 'digest'
			));
	
		if ($this->session->userdata('order_id')) {
            $cart_detail = $this->order_model->get_order_details($this->session->userdata('order_id'));
        } else {
            $this->session->set_flashdata('error_msg' , $data_json->message);
			redirect('takeaway/checkout');
        }
		 
		$basket = array();
		$amount = 0;
		foreach($cart_detail as $item) { 
			$basket[] = array(
				"id" => $item->id,
				"price_id" => $item->price_id,
				"item_id" => $item->item_id,
				"quantity" => $item->qty,
				"special_instructions" => "not available"
			);
			$amount = $amount + ($item->qty * $item->price);
		}
		
		$order_detail = $this->session->userdata('order_detail');
		$logged_user = $this->session->userdata('logged_user');
		$data = array(
			'customer_id' =>  $logged_user['customer_id'],
			'credit_card_id' => $credit_card_id,
			"address_id" =>$order_detail['address_id'],
			"amount" =>$amount,
			"tax_rate" => 0.00,
			"tax" => 0.00,
			"shipping_type" => "Free",
			"shipping_charges" => 0.00,
			"discount" => 0.00,
			//'order_type'=> $order_detail['order_type'],
			'payment_method' => $payment_method,
			'basket' => json_encode($basket)
		);
		
		
		
		$save_order = $this->rest->post('orders/order/venue_id/'. $this->config->item('venue_id') . "/", $data);
		
			
		
		if($save_order->status) {
			$this->session->set_userdata('current_order' , $save_order->data->order_id);
			$this->session->set_flashdata('msg' , "Yout Order is pending, ");
			redirect('takeaway/order_complete');
		} else { 
			$this->session->set_flashdata('error_msg' , $save_order->msg);
			redirect('takeaway/place_order');
		}
	}
   
}
