<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }
	
	public function register() {
		
	 $venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }
        
        $data = array(
            'title' => 'Regsiter',
            'view' => 'register',
            'venues' => $venue['data']
        );
        $this->load->view('theme/contact_theme', $data);
	}
	
	function user_logedin() { 
		$customer = json_decode(file_get_contents($this->config->item('api_url') . "customer/get/customer_id/" . $this->uri->segment(3)), TRUE);
        if(!$customer['status']) { 
			show_404();
		}
		
		 $data = array(
			"customer_id" => $customer['customer']['customer_id'],
            'email' => $customer['customer']['email'],
            "first_name" => $customer['customer']['first_name'],
            "mobile" => $customer['customer']['mobile_number'],
            "address" => $customer['customer']['address'],
            'is_logedin' => 1
		 );
		  $this->session->set_userdata('logged_user' , $data);
		 $this->session->set_flashdata('msg' , 'Congratulation you have been successfully created Baan Siam account!');
		 redirect('user/confirm_address');
	}
	
	public function confirm_address() {
		$logged_user = $this->session->userdata('logged_user');
		if($logged_user['is_logedin'] == false) {
			redirect('takeaway/checkout');
		}	
		
		$venue = json_decode(file_get_contents($this->config->item('api_url') . 'home/venue/venue_id/' . $this->config->item('venue_id')), TRUE);
        if (!$venue['status']) {
            show_404();
        }
		
		$customer = json_decode(file_get_contents($this->config->item('api_url') . 'customer/get/customer_id/' . $logged_user['customer_id']), TRUE);
        if(!$customer['status']) { 
			show_404();
		}
		
		$customer_address = json_decode(file_get_contents($this->config->item('api_url') . 'customer/get_customer_all_address/customer_id/' . $logged_user['customer_id']), TRUE);
        if(!$customer['status']) { 
			show_404();
		}
		
		//print_r($customer_address); exit;
        $data = array(
            'title' => 'Confirm Address',
            'view' => 'confirm_address',
            'venues' => $venue['data'],
			'customer' => $customer['data'],
			'addresses' => $customer_address['data']
        );
        $this->load->view('theme/contact_theme', $data);
	}
	
	
	
	public function register_user() { 
		
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'phone' => $this->input->post('phone'),
			'address' => $this->input->post('address'),
			'password' => $this->input->post('password')
		);
		
		$query = $this->user_model->register($data);
		if($query) { 
			$this->session->set_flashdata('msg' , 'Thank you for Registraton! Please Login to made Order.');
		} else { 
			$this->session->set_flashdata('msg' , 'Something Error! Registraton Failed.');
		}
		redirect('user/register');
	}
	
		public function login() { 
		$data = array(
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password')
		);
		
		$query = $this->user_model->login($data);
		if($query) { 
			redirect('takeaway/menu');
		} else { 
			$this->session->set_flashdata('msg' , 'Something Error! Login Failed.');
			redirect('user/register');
		}
	}
	public function logout() { 
		$this->session->sess_destroy();
		redirect('takeaway');
	} 
}


