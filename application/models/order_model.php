<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Order_model extends CI_Model {
  
  public function insert_order($data) { 
	$this->db->insert('orders' , $data);
	return $this->db->insert_id();
  }
  
  public function insert_order_detail($data) { 
	$this->db->insert('order_detail', $data);
  }
  
  public function update_order_detail($id, $data) { 
	$this->db->where('id' , $id);
	$this->db->update('order_detail', $data);
  }
  
  public function get_order_details($order_id) { 
	$this->db->where('order_id', $order_id);
	$query = $this->db->get('order_detail');
	return $query->result();
  }
  
  public function get_order_detail($id) { 
	$this->db->where('order_id', $this->session->userdata('order_id'));
	$this->db->where('id' ,$id );
	$query = $this->db->get('order_detail');
	return $query->row();
  }
  
   public function delete_order_details($order_id , $type) { 
	$this->db->where('id', $order_id);
	$this->db->where('order_item', $type);
	 $this->db->delete('order_detail');
  }
}


