/*
Navicat MySQL Data Transfer

Source Server         : takeaway
Source Server Version : 50529
Source Host           : usmanfaisal.com:3306
Source Database       : takeaway

Target Server Type    : MYSQL
Target Server Version : 50529
File Encoding         : 65001

Date: 2014-11-16 23:32:01
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `ci_sessions`
-- ----------------------------
DROP TABLE IF EXISTS `ci_sessions`;
CREATE TABLE `ci_sessions` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(45) NOT NULL DEFAULT '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ci_sessions
-- ----------------------------
INSERT INTO `ci_sessions` VALUES ('19e93ff00fdea2d8304ce2be0262103c', '80.45.171.17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36', '1416054352', 'a:3:{s:9:\"user_data\";s:0:\"\";s:11:\"logged_user\";a:6:{s:11:\"customer_id\";s:3:\"106\";s:5:\"email\";s:28:\"orders@becreativegroup.co.uk\";s:10:\"first_name\";s:7:\"Andreas\";s:6:\"mobile\";s:13:\"+448454749192\";s:7:\"address\";s:0:\"\";s:10:\"is_logedin\";i:1;}s:12:\"order_detail\";a:8:{s:11:\"customer_id\";s:0:\"\";s:4:\"name\";b:0;s:6:\"mobile\";b:0;s:7:\"address\";b:0;s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:6:\"d_time\";s:0:\"\";s:4:\"note\";b:0;}}');
INSERT INTO `ci_sessions` VALUES ('22d69726c3251dac9b6701ba87c4b03b', '80.45.171.17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36', '1416053200', 'a:2:{s:9:\"user_data\";s:0:\"\";s:8:\"order_id\";i:20;}');
INSERT INTO `ci_sessions` VALUES ('774ea0ee75cf58492d36c7342ef3f105', '182.185.215.103', 'Mozilla/5.0 (Windows NT 6.3; WOW64; rv:33.0) Gecko/20100101 Firefox/33.0', '1416155966', '');
INSERT INTO `ci_sessions` VALUES ('ed634c292f8a3053f4d82b8b7b7bad3f', '80.45.171.17', 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/38.0.2125.111 Safari/537.36', '1416053125', '');

-- ----------------------------
-- Table structure for `order_detail`
-- ----------------------------
DROP TABLE IF EXISTS `order_detail`;
CREATE TABLE `order_detail` (
  `id` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `price` double(5,2) NOT NULL,
  `image` varchar(250) NOT NULL,
  `option` varchar(255) NOT NULL,
  `extra` varchar(100) NOT NULL,
  `description` varchar(500) NOT NULL,
  `featured` varchar(2) NOT NULL,
  `serving_size` varchar(20) NOT NULL,
  `qty` int(2) NOT NULL,
  `total_price` double(5,2) NOT NULL,
  `order_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of order_detail
-- ----------------------------
INSERT INTO `order_detail` VALUES ('80_0', 'Chicken Satay - - NEW!', '4.55', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/1411556886236689385_l.jpg', '', '', 'Marinated deep fried chicken morsals with our own satay dip.', '1', '1 person', '3', '0.00', '16');
INSERT INTO `order_detail` VALUES ('64_0', 'Duck Spring Rolls - - NEW', '4.95', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115562621555518808_l.jpg', '', '', 'Crispy vegetable & duck spring rolls. Home made Thai style served with \nhoisin dipping sauce', '1', '1 person', '3', '14.85', '16');
INSERT INTO `order_detail` VALUES ('4_16_11', 'Spaghetti Kii Mao Neau', '14.00', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/1411551431696237591_l.jpg', 'test:12', 'test:2 ', 'Spaghetti stir fried with beef, herbs & spices, onions, chillis, bell peppers, \ndwarf beans, & carrot.', '1', '1 Person', '1', '14.00', '17');
INSERT INTO `order_detail` VALUES ('9_0', 'Spaghetti Phad Gaeng Daeng Gai ', '7.35', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115515941994849506_l.jpg', '', '', ' Stir fried spaghetti with chicken in red curry & accompanying vegetables.', '1', '1 person', '1', '7.35', '17');
INSERT INTO `order_detail` VALUES ('8_0', 'Spaghetti Phad Gaeng Daeng Koong', '7.85', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115515741474020553_l.jpg', '', '', ' Stir fried spaghetti with tiger prawns in red curry & accompanying \nvegetables.', '1', '1 person', '1', '7.85', '17');
INSERT INTO `order_detail` VALUES ('7_0', 'Spaghetti Phad Gaeng Kiaw Wan Gai', '7.35', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/1411551559309976329_l.jpg', '', '', ' Stir fried spaghetti with chicken in green curry & accompanying \nvegetables', '0', '1 person', '1', '7.35', '17');
INSERT INTO `order_detail` VALUES ('6_0', 'Spaghetti Phad Gaeng Kiaw Wan Koong', '7.85', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115515401498056244_l.jpg', '', '', 'Stir fried spaghetti with tiger prawns in green curry & accompanying \nvegetables.', '1', '1 Person', '1', '7.85', '17');
INSERT INTO `order_detail` VALUES ('5_0', 'Spaghetti Phad Kra Pao Gai/Moo', '7.35', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115515191963190446_l.jpg', '', '', 'Stir fried spaghetti with chicken or minced pork with herbs & spices, chillis \n& vegetables.', '0', '1 Person', '1', '7.35', '17');
INSERT INTO `order_detail` VALUES ('10_0', 'Spaghetti Phad Priaw Wan Koong ', '7.85', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115516141840105082_l.jpg', '', '', 'Stir fried sweet & sour spaghetti with tiger prawns, pineapple & bell \npeppers', '0', '1 person', '2', '15.70', '17');
INSERT INTO `order_detail` VALUES ('95_0', 'Gaeng Kari Koong ', '8.90', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/1411557520717419570_l.jpg', '', '', 'Tiger Prawns served in a creamy yellow curry prepared with spices, \ncoconut milk, fresh herbs & vegetables.', '1', '1 person', '1', '8.90', '17');
INSERT INTO `order_detail` VALUES ('33_0', 'Gaeng Kari Tao Hoo', '8.30', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115542731899087883_l.jpg', '', '', 'Tofu served in a creamy yellow curry prepared with coconut milk, fresh \nherbs & spices, bell peppers potatoes, carrot slivers & onions', '1', '1 person', '2', '16.60', '17');
INSERT INTO `order_detail` VALUES ('93_0', 'Gaeng Kiaw Wan Koong ', '8.90', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/141155742363919260_l.jpg', '', '', 'Tiger Prawns served in a velvety green curry made with coconut milk, \nbamboo shoots, fresh herbs & veg.', '1', '', '3', '0.00', '17');
INSERT INTO `order_detail` VALUES ('64_0', 'Duck Spring Rolls - - NEW', '4.95', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115562621555518808_l.jpg', '', '', 'Crispy vegetable & duck spring rolls. Home made Thai style served with \nhoisin dipping sauce', '1', '1 person', '3', '14.85', '18');
INSERT INTO `order_detail` VALUES ('95_0', 'Gaeng Kari Koong ', '8.90', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/1411557520717419570_l.jpg', '', '', 'Tiger Prawns served in a creamy yellow curry prepared with spices, \ncoconut milk, fresh herbs & vegetables.', '1', '1 person', '1', '8.90', '18');
INSERT INTO `order_detail` VALUES ('33_0', 'Gaeng Kari Tao Hoo', '8.30', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115542731899087883_l.jpg', '', '', 'Tofu served in a creamy yellow curry prepared with coconut milk, fresh \nherbs & spices, bell peppers potatoes, carrot slivers & onions', '1', '1 person', '1', '8.30', '18');
INSERT INTO `order_detail` VALUES ('93_0', 'Gaeng Kiaw Wan Koong ', '8.90', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/141155742363919260_l.jpg', '', '', 'Tiger Prawns served in a velvety green curry made with coconut milk, \nbamboo shoots, fresh herbs & veg.', '1', '', '1', '8.90', '18');
INSERT INTO `order_detail` VALUES ('99_0', 'Gaeng Paa Koong', '8.90', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/1411557842141668789_l.jpg', '', '', 'Tiger prawns served in “Jungle Curry”. This extremely spicy dish does not \ncontain coconut milk and is prepared with fresh herbs & spices, bamboo \nshoots, mushrooms, dwarf beans & chillis. Milder version prepared on \nrequest!!', '1', '1 person', '1', '8.90', '18');
INSERT INTO `order_detail` VALUES ('80_0', 'Chicken Satay - - NEW!', '4.55', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/1411556886236689385_l.jpg', '', '', 'Marinated deep fried chicken morsals with our own satay dip.', '1', '1 person', '1', '4.55', '19');
INSERT INTO `order_detail` VALUES ('64_0', 'Duck Spring Rolls - - NEW', '4.95', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115562621555518808_l.jpg', '', '', 'Crispy vegetable & duck spring rolls. Home made Thai style served with \nhoisin dipping sauce', '1', '1 person', '3', '14.85', '19');
INSERT INTO `order_detail` VALUES ('93_0', 'Gaeng Kiaw Wan Koong ', '8.90', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/141155742363919260_l.jpg', '', '', 'Tiger Prawns served in a velvety green curry made with coconut milk, \nbamboo shoots, fresh herbs & veg.', '1', '', '1', '8.90', '19');
INSERT INTO `order_detail` VALUES ('125_0', 'Gai Phad Med Mamuang Himmapan ', '8.25', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/141155961399038232_l.jpg', '', '', 'Chicken with cashew nuts, spring onions, onions, bell pepper, diced carrot \n& grilled chillies.', '0', '1 person', '1', '8.25', '20');
INSERT INTO `order_detail` VALUES ('124_0', 'Phad Cha Gai', '8.25', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115594841033959693_l.jpg', '', '', 'Very spicy chicken with galangal, kaffir lime leaves, kra chai, bamboo \nshoots, peppers, carrot, beans & chillis.', '1', '1 person', '1', '8.25', '20');
INSERT INTO `order_detail` VALUES ('123_0', 'Phad Cha Koong ', '8.85', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/1411559455178430281_l.jpg', '', '', 'Very spicy tiger prawns with galangal, kaffir lime leaves, kra chai, \nbamboo shoots, peppers, carrot, beans & chillis.', '0', '1 person', '1', '8.85', '20');
INSERT INTO `order_detail` VALUES ('122_0', 'Phad Cha Pla', '9.30', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/141155941932492175_l.jpg', '', '', 'Very spicy fish (hake) with galangal, kaffir lime leaves, kra chai, bamboo \nshoots, bell peppers, chillis, carrot & dwarf beans.', '0', '1 person', '1', '9.30', '20');
INSERT INTO `order_detail` VALUES ('110_0', 'Phad Gai/Neau/Moo Nam Man Hoi ', '8.25', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115587461205669118_l.jpg', '', '', 'Your choice of chicken or beef or pork in oyster sauce with fresh \nvegetables.', '1', '1 person', '1', '8.25', '20');
INSERT INTO `order_detail` VALUES ('16_0', ' Raad Nah Gai/Moo - - New Variation! ', '7.35', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115518531024872365_l.jpg', '', '', 'Stir fried rice ribbon noodles or egg noodles with your choice ofchicken or \npork with egg, mixed vegetables of the day & gravy', '0', '1 person', '1', '7.35', '20');
INSERT INTO `order_detail` VALUES ('21_0', 'Khow Phad Koong + Khai', '6.95', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/14115522351873303889_l.jpg', '', '', 'Fragrant tiger prawn fried rice with egg & mixed chopped vegetables \nand fish sauce.', '0', '1 person', '1', '6.95', '20');
INSERT INTO `order_detail` VALUES ('24_0', 'Khow Phad Kra Pao Koong', '6.95', 'http://usmanfaisal.com/maduber/assets/uploads/takeaway/141155238193012013_l.jpg', '', '', 'Spicy tiger prawn fried rice with basil, mushroom, chillis & bell peppers and \nfish sauce ', '0', '1 person', '1', '6.95', '20');

-- ----------------------------
-- Table structure for `orders`
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('16', 'ARfan');
INSERT INTO `orders` VALUES ('17', 'ARfan');
INSERT INTO `orders` VALUES ('18', 'ARfan');
INSERT INTO `orders` VALUES ('19', 'ARfan');
INSERT INTO `orders` VALUES ('20', 'ARfan');
